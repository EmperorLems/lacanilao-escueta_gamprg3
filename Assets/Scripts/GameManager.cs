using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.TerrainAPI;

public class GameManager : MonoBehaviour
{
    public Action<bool> OnEnteredFiveMinutes;
    public Action<bool> OnEntered30Seconds;

    [SerializeField] private UIManager uiManager;
    [SerializeField] private UnitHUD unitHUD;
    [SerializeField] private AbilityHUD abilityHUD;
    [SerializeField] private GameObject debugPanel;
    [SerializeField] private GameOverScreen GameoverScreen;
    [SerializeField] private UnitManager unitManager;
    [SerializeField] private Minimap minimap;
    [SerializeField] private GameObject scoreboardPanel;
    [SerializeField] private ScoreBoard scoreboardScript;
    [SerializeField] private effectIconHud effectIconHud;

    private float gameTime;
    private bool hasEnteredFiveMins = true;
    private bool hasEntered30Seconds = false;

    [HideInInspector]
    public static float minutes;
    public static float seconds;

    [HideInInspector]
    public bool isDay = false;
    [Header("Radiant Spanwers")]
    [SerializeField] private List<HeroSpawner> radiantSpawners;
    [Header("Dire Spanwers")]
    [SerializeField] private List<HeroSpawner> direSpawners;

    [SerializeField] private PlayerController player;

    [Header("Damage Popup")]
    [SerializeField] private DamageText DamageText_Prefab;

    [Header("Layers")] 
    public LayerMask UnitsLayers;
    public LayerMask UnitsTowersLayer;
    public LayerMask TowersLayer;
    public LayerMask ClickableLayer;
    public LayerMask ignoreRaycastLayer;
    
    public static GameManager instance;
    public int direScore = 0;
    public int radiantScore = 0;
    public Action<Hero> OnPlayerAdd;
    private void Awake()
    {
        instance = this;
        OnGameStart();
        //DebugSelectedHeroes();
        //CombatTesting();
    }

    void Start()
    {
        Time.timeScale = 1.0f;

        if (player == null)
        {
            player = FindObjectOfType<PlayerController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        SetTime();

        //for Testing Remove after checking
        Add5Minutes();
        //

        TimerTracker();

        ShowDebug();
        
        ScoreBoard();
    }

    private void OnGameStart()
    {
        int direCounter = 0;
        foreach (Hero direHero in SelectedHeroes.DireHeroes)
        {
            if (direSpawners[direCounter].isMaxHero) direCounter++;
            direSpawners[direCounter].UnitManager = unitManager;
            direSpawners[direCounter].SpawnHero(direHero, false);
        }
        
        int radiantCounter = 0;
        foreach (Hero radiantHero in SelectedHeroes.RadiantHeroes)
        {
            if (radiantSpawners[radiantCounter].isMaxHero) radiantCounter++;
            radiantSpawners[radiantCounter].UnitManager = unitManager;
            radiantSpawners[radiantCounter].SpawnHero(radiantHero, false);
        }
        
        Hero hero = radiantSpawners[2].SpawnHero(SelectedHeroes.playerHero, true);
        player = hero.GetComponent<PlayerController>();
        //OnPlayerAdd.Invoke(hero);

    }

    void DebugSelectedHeroes()
    {
        foreach (Hero dire in SelectedHeroes.DireHeroes)
        {
            Debug.Log(dire.faction + " +++ " + dire);
        }
        
        foreach (Hero rad in SelectedHeroes.RadiantHeroes)
        {
            Debug.Log(rad.faction + " --- " + rad);
        }

        Hero g = SelectedHeroes.playerHero;
        Debug.Log(g.faction + " --- " + g);
    }

    void CombatTesting()
    {
        player = FindObjectOfType<PlayerController>();
        unitManager.AddHeroToList(player.GetComponent<Hero>());
        Hero enemy = FindObjectOfType<HeroController>().GetComponent<Hero>();
        unitManager.AddHeroToList(enemy);
    }
    private void TimerTracker()
    {
        if (minutes % 5 == 0 && seconds == 0 && minutes > 0)
        {
            if (hasEnteredFiveMins == true)
            {
                OnEnteredFiveMinutes?.Invoke(true);
                hasEnteredFiveMins = false;
            }
        }
        else if (seconds % 30 == 0 && minutes > 0)
        {
            if (hasEntered30Seconds == true)
            {
                OnEntered30Seconds?.Invoke(true);
                hasEntered30Seconds = false;
            }
        }
        else
        {
            hasEnteredFiveMins = true;
            hasEntered30Seconds = true;
        }

        if (minutes % 3 == 0 && seconds == 0 && minutes > 0)
        {
            unitManager.ScaleCreeps();
        }
    }

    public void SpeedTime()
    {
        Time.timeScale = 2;
    }

    public void RevertTimeToNormal()
    {
        Time.timeScale = 1;
    }

    public void SetTime()
    {
        gameTime += 1 * Time.deltaTime;

        seconds = Mathf.FloorToInt(gameTime % 60);
        minutes = Mathf.FloorToInt(gameTime / 60);

        uiManager.SetTime(gameTime);
    }

    #region ForTesting

    private void ShowDebug()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            debugPanel.SetActive(!debugPanel.activeSelf);

            //Debug.Log("Lumabas and debug");
        }
    }

    private void Add5Minutes()
    {
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            gameTime += 60;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            gameTime += 50;
        }
    }

    #endregion
    

    public void DamagePopup(string message, Vector3 mLocation, Color txtColor, float txtScale = 1)
    {
        DamageText dmgTXT = Instantiate(DamageText_Prefab, mLocation, Camera.main.transform.rotation);
        dmgTXT.SetText(message);
        dmgTXT.transform.SetParent(transform, true);
        dmgTXT.startingColor = txtColor;
        dmgTXT.gameObject.transform.localScale *= txtScale;
    }

    public void GameOver(bool didPlayerWin)
    {
        Instantiate(GameoverScreen).setUp(didPlayerWin);
    }
    
    public void ScoreBoard()
    {
        if(Input.GetKeyDown(KeyCode.Tab)) scoreboardPanel.SetActive(true);
        if(Input.GetKeyUp(KeyCode.Tab)) scoreboardPanel.SetActive(false);
    }
    public UIManager GetUIManager() => uiManager;
    public UnitHUD GetUnitHUD() => unitHUD;
    public AbilityHUD getAbilityHUD() => abilityHUD;
    public UnitManager getUnitManager() => unitManager;
    public Minimap getMinimap() => minimap;
    public ScoreBoard getScoreBoard() => scoreboardScript;
    public PlayerController Player => player;
    public effectIconHud EffectIconHud => effectIconHud;

    public bool isInUnitsLayer(GameObject obj) => ((UnitsLayers.value & (1 << obj.layer)) > 0);

    public bool isInClickableLayer(GameObject obj) => ((ClickableLayer.value & (1 << obj.layer)) > 0);

    public bool isInUnitsAndTowerLayer(GameObject obj) => ((UnitsTowersLayer.value & (1 << obj.layer)) > 0);
    public bool isTowerLayer(GameObject obj) => ((TowersLayer.value & (1 << obj.layer)) > 0);
}
