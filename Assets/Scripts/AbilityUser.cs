using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbilityUser : MonoBehaviour
{
    [SerializeField] public AbilityBase[] abilities = new AbilityBase[4];
    [SerializeField] private AbilityBase Test1;
    [SerializeField] private AbilityBase Test2;
    [SerializeField] private AbilityBase Test3;
    [SerializeField] private AbilityBase Test4;

    private Hero hero;
    private AbilityBase activeAbility;
    private int MaxBasicAbilityUpgrade = 1;
    private int MaxUltimateBasicAbilityUpgrade = 1;

    [HideInInspector] public UnityEvent<AbilityBase, int> OnAddAbility;

    private GameObject container;
    private void Awake()
    {
        hero = GetComponent<Hero>();
        hero.GetLevelComponent().OnBasicAbilityUpgradable.AddListener(lvlComp =>
        {
            MaxBasicAbilityUpgrade++;
        });
        
                
        hero.GetLevelComponent().OnUltimateAbilityUpgradable.AddListener(lvlComp =>
        {
            MaxUltimateBasicAbilityUpgrade++;
        });
        
        hero.OnUnitDeath.AddListener((arg0, o) =>
        {
            CancelActiveAbilities();
        });
    }

    void Start()
    {
        container = new GameObject("Ability Container");
        container.transform.parent = hero.transform;
        container.transform.localPosition = Vector3.zero;
        
        AddAbility(Test1);
        AddAbility(Test2);
        AddAbility(Test3);
        AddAbility(Test4);
    }

    public void CancelActiveAbilities()
    { 
        foreach (AbilityBase ability in abilities)
        {
            if (ability != null && !ability.IsPassive)
            {
                ability.Cancel();
            }
        }

        if (hero.state == UnitState.SpecialAttack || hero.state == UnitState.Channeling)
        {
            hero.state = UnitState.Idle;
        } activeAbility = null;
    }
    
    public void UseAbility(int index)
    {
        AbilityBase ability = abilities[index];
        if (ability!=null && !ability.IsPassive)
        {
            if (isEnoughMana(ability))
            {
                if (abilities[index].GetAbilityState == AbilityState.Standby)
                {
                    CancelActiveAbilities();
                    activeAbility = ability;
                    
                    ability.OnFinishTargeting.AddListener(ab =>
                    {
                        if (ab == ability)
                        {
                            hero.state = UnitState.SpecialAttack;
                        }
                    });
                    
                    ability.Activate();
                }
            }
            else
            {
                if (hero.GetComponent<PlayerController>() != null)
                {
                    GameManager.instance.GetUnitHUD().OnNotEnoughMana();
                }
            }
        }
    }

    public void AddAbility(AbilityBase _ability)
    {
        for (int i = 0; i < abilities.Length; i++)
        {
            if (abilities[i] == null)
            {
                AbilityBase a = Instantiate(_ability, container.transform);
                abilities[i] = a;
                a.Setup(hero, this);

                OnAddAbility.Invoke(a,i);
                
                if(a.IsPassive) a.Activate();
                return;
            }
        }
    }

    private bool isEnoughMana(AbilityBase _ability)
    {
        return _ability.ManaCost <= hero.GetManaComponent().GetCurrentMana();
    }

    private void OnFinishAnimation()
    {
        if(activeAbility == null) return;
        activeAbility.AbilityAttack();
        activeAbility = null;
    }

    // returns bool if upgrading is successful or not 
    public bool UpgradeAbility(int index)
    {
        AbilityBase ability = abilities[index];
        
        if (isAbilityUpgradable(index))
        {
            if (hero.TryGetComponent(out LevelingComponent lvl))
            {
                if (lvl.currentSkillPoints > 0)
                {
                    ability.UpgradeAbility();
                    lvl.UseSkillPoint();
                    return true;
                }
            }
        }

        return false;
    }

    public bool isAbilityUpgradable(int index)
    {
        AbilityBase ability = abilities[index];
        if (ability == null) return false;

        if (ability.IsUltimate)
        {
            if (!ability.IsMaxUpgraded() && ability.Level < MaxUltimateBasicAbilityUpgrade)
            {
                return true;
            }
        }
        if (!ability.IsMaxUpgraded() && ability.Level < MaxBasicAbilityUpgrade)
        {
            return true;
        }
        return false;
    }

    public void DispelAbilities()
    {
        foreach (AbilityBase ability in abilities)
        {
            ability.Dispel();
        }
    }

    public void OnUserDeath()
    {
        
    }
}
