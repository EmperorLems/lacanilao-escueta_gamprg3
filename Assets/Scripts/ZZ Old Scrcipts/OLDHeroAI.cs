using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class OLDHeroAI : MonoBehaviour
{
    // [SerializeField] private Hero hero;
    // [SerializeField] private Animation animator;
    // [SerializeField] private BoxCollider AttackZone;
    //
    // //private UnitMovement movement;
    // private GameObject currentTarget;
    // private bool isAttacking;
    //
    //
    // private List<GameObject> nearbyEnemies = new List<GameObject>();
    //
    // void Start()
    // {
    //     // movement = hero.getUnitMovement();
    //     hero.state = UnitState.Follow;
    // }
    //
    // void FixedUpdate()
    // {
    //     switch (hero.state)
    //     {
    //         case UnitState.Follow: FollowWaypoint();
    //             break;
    //         case UnitState.Chase: ChaseTarget();
    //             break;
    //         case UnitState.Attack: Attack();
    //             break;
    //         case UnitState.Stop: Stop();
    //             break;
    //         default: Idle();
    //             break;
    //     }
    // }
    //
    //
    // #region State Updates
    //
    // void Idle()
    // {
    //     animator.Play("free");
    //     movement.Stop();
    // }
    //
    // void Attack()
    // {
    //     if (currentTarget == null)
    //     {
    //         hero.state = UnitState.Follow;
    //         return;
    //     }
    //     
    //     if (movement.isTargetInFront(currentTarget.transform.position, 8.0f))
    //     {
    //         float attacksPerSecond = hero.GetAttackSpeed() / (100 * hero.GetAttackTime());
    //
    //         float interval = 1 / attacksPerSecond;
    //
    //         animator["BarbarianAttack"].speed = animator["BarbarianAttack"].clip.length /interval;
    //     
    //         if(!animator.IsPlaying("BarbarianAttack")) animator.Play("BarbarianAttack");
    //         if(isAttacking) return;
    //     }
    //     else
    //     {
    //         hero.state = UnitState.Chase;
    //     }
    // }
    //
    // void ChaseTarget()
    // {
    //     animator.Play("walk");
    //     if (currentTarget == null)
    //     {
    //         hero.state = UnitState.Follow;
    //         return;
    //     }
    //     
    //     movement.MoveToTarget(currentTarget.transform.position);
    //     
    //     // check if Target is infront
    //     if(movement.isTargetInFront(currentTarget.transform.position, 10.0f)) 
    //         hero.state = UnitState.Attack;
    // }
    //
    // private void FollowWaypoint()
    // {
    //     if (movement.isNearToWaypoint) hero.state = UnitState.Chase;
    //     animator.Play("walk");
    //     movement.MoveToWaypoint();
    // }
    //
    // private void Stop()
    // {
    //     animator.Play("free");
    //     movement.Stop();
    // }
    //
    // #endregion
    //
    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.gameObject.layer == 6 || other.gameObject.layer == 7|| other.gameObject.layer == 10)
    //     {
    //         if (other.gameObject.GetComponent<Unit>().faction != hero.faction)
    //         {
    //             AddToList(other.gameObject);
    //         }
    //     } 
    //     if (other.gameObject.GetComponent<Tower>())
    //     {
    //         if (other.gameObject.GetComponent<Tower>().faction != hero.faction)
    //         {
    //             AddToList(other.gameObject);
    //         }
    //     }
    // }
    //
    // private void OnTriggerExit(Collider other)
    // {
    //     if (other.gameObject.layer == 6 || other.gameObject.layer == 7|| other.gameObject.layer == 10)
    //     {
    //         if (other.gameObject.GetComponent<Unit>().faction != hero.faction)
    //         {
    //             RemoveFromList(other.gameObject);
    //         }
    //     } 
    //     if (other.gameObject.GetComponent<Tower>())
    //     {
    //         if (other.gameObject.GetComponent<Tower>().faction != hero.faction)
    //         {
    //             RemoveFromList(other.gameObject);
    //         }
    //     }
    // }
    //
    // void AddToList(GameObject obj)
    // {
    //     if (!nearbyEnemies.Contains(obj))
    //     {
    //         if (nearbyEnemies.Count == 0)
    //         {
    //             currentTarget = obj;
    //             hero.state = UnitState.Chase;
    //         }
    //         nearbyEnemies.Add(obj);
    //         
    //     }
    // }
    //
    // void RemoveFromList(GameObject obj)
    // {
    //     if (nearbyEnemies.Contains(obj))
    //     {
    //         nearbyEnemies.Remove(obj);
    //         if (obj == currentTarget)
    //         {
    //             if (nearbyEnemies.Count > 0)
    //             {
    //                 currentTarget = nearbyEnemies[0];
    //                 hero.state = UnitState.Chase;
    //             }
    //             else
    //             {
    //                 hero.state = UnitState.Follow;
    //                 currentTarget = null;
    //             }
    //         }
    //     }
    // }
    //
    // IEnumerator AttackRoutine()
    // {
    //     if (!isAttacking)
    //     {
    //         isAttacking = true;
    //         animator.Play("BarbarianAttack");
    //         yield return new WaitForSeconds(hero.getAttackInterval());
    //         isAttacking = false;
    //     }
    // }
    // public void OnMeleeAttack()
    // {
    //     if (AttackZone.bounds.Contains(currentTarget.transform.position))
    //     {
    //         if (currentTarget.GetComponent<Unit>())
    //         {
    //             currentTarget.GetComponent<DamageReceiver>().TakeDamage(hero.GetDamage(),AttackType.Hero, this.gameObject);
    //         }
    //         else if (currentTarget.GetComponentInParent<Tower>())
    //         {
    //             currentTarget.GetComponentInParent<DamageReceiver>().TakeDamage(hero.GetDamage(),AttackType.Hero, this.gameObject);
    //         }
    //     }
    // }

}
