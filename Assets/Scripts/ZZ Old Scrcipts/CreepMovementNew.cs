using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepMovementNew : MonoBehaviour
{
    [SerializeField] private List<Transform> waypoints;

    [SerializeField] private LayerMask layer;
    [SerializeField] private float speed = 100;
    [SerializeField] private float minimumDistToAvoid = 0;
    //[SerializeField] private bool goingTowards = true;

    //Actual speed of the creep 
    private float curSpeed;
    private float initialSpeed;

    [SerializeField] private float multiplier;
    [SerializeField] private float rotSpeed;

    Vector3 dir;
    Vector3 destPos;

    private void Awake()
    {
        initialSpeed = speed;
        curSpeed = initialSpeed;
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Movement

    public Vector3 AvoidObstacles()
    {
        RaycastHit hit;

        Ray ray = new Ray(transform.position, transform.forward);
        Debug.DrawRay(transform.position, transform.forward);

        Ray sideRay = new Ray(transform.position, transform.right);
        Debug.DrawRay(transform.position, transform.right);

        Ray sideRay2 = new Ray(transform.position, transform.right * -1);
        Debug.DrawRay(transform.position, transform.right * -1);

        if (Physics.Raycast(ray, out hit, minimumDistToAvoid, layer.value))
        {
            return hit.normal * speed * multiplier;
        }
        if (Physics.Raycast(sideRay, out hit, minimumDistToAvoid, layer.value))
        {
            return hit.normal * speed * multiplier;
        }
        if (Physics.Raycast(sideRay2, out hit, minimumDistToAvoid, layer.value))
        {
            return hit.normal * speed * multiplier;
        }
        else
        {
            return new Vector3(0.0f, 0.0f, 0.0f);
        }
    }

    public void MoveForward()
    {
        if (waypoints.Count > 0)
        {
            destPos = waypoints[0].transform.position;
            dir = destPos - this.transform.position;
            dir += AvoidObstacles();
            dir.Normalize();
            Quaternion targetRotation = Quaternion.LookRotation(dir);
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, targetRotation, Time.deltaTime * rotSpeed);

            transform.position += transform.forward * curSpeed * Time.deltaTime;
        }
    }

    public void MoveToTarget(Vector3 target)
    {
        if (target != null)
        {
            destPos = target;
            dir = destPos - this.transform.position;
            dir += AvoidObstacles();
            dir.Normalize();
            Quaternion targetRotation = Quaternion.LookRotation(dir);
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, targetRotation, Time.deltaTime * rotSpeed);


            transform.position += transform.forward * speed * Time.deltaTime;
        }
    }
    
    public void SetWaypoint(List<Transform> towerWaypoints)
    {
        waypoints = towerWaypoints;
    }

    #endregion

    public void ResetSpeed()
    {
        curSpeed = initialSpeed;
    }

    public void Stop()
    {
        curSpeed = 0;
    }

    public bool isNearbyToWaypoint()
    {
        return Vector3.Distance(this.transform.position, waypoints[0].position) <= 15.0f;
    }
    
    public bool IsTargetInFront(Vector3 targetLocation, float acceptableDistance)
    {
        // RaycastHit hit;
        // Ray ray = new Ray(transform.position, transform.forward);
        // Debug.DrawRay(transform.position, transform.forward);
        //
        // if (Physics.Raycast(ray, out hit, 5.0f, layer.value))
        // {
        //     if (hit.transform.gameObject == currentTarget)
        //     {
        //         return true;
        //     }
        // }
        
        if (Vector3.Distance(this.transform.position, targetLocation) > acceptableDistance)
        {
            return false;
        }
        
        Vector3 relativePos = targetLocation - transform.position;
        
        return Vector3.Dot(transform.forward, relativePos) > 0.0f;
    }
}
