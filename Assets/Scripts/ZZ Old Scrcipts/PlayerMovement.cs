using UnityEngine;
using System.Collections;
using System;

public class PlayerMovement : MonoBehaviour 
{
    public LayerMask layer;
    public float speed = 20.0f;
    public float mass = 5.0f;
    public float force = 50.0f;
    public float minimumDistToAvoid = 20.0f;
    public bool goingTowards=true;
    //Actual speed of the vehicle 
    private float curSpeed;
    private Vector3 targetPoint;
	private float initialSpeed;
    public float multiplier;

	void Start () 
    {
        mass = 5.0f;
        targetPoint = this.transform.position;
		initialSpeed = speed;
	}

    void OnGUI()
    {
        GUILayout.Label("Click anywhere to move the vehicle to the clicked point");
    }
	
	void Update () 
    {
        //Vehicle move by mouse click
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 50))
        {
            goingTowards = true;
            speed = initialSpeed;
            targetPoint = hit.point;
        }
        if (Input.GetMouseButton(1) && Physics.Raycast(ray, out hit, 50))
        {
            goingTowards = false;
            speed = -initialSpeed;
            //targetPoint = -hit.point;
        }

        Vector3 dir = new Vector3(0.0f, 0.0f, 0.0f);


        //1- Compute the directional vector to the target position
        dir = new Vector3(targetPoint.x, 0, targetPoint.z) - transform.position;

        //2- Exit the update function, so that the vehicle stops when the target point is 1 meter away
        if (goingTowards==true)
        {
            if (dir.magnitude < 0.3f)
            {
                speed = initialSpeed;
                return;
            }
            else if (dir.magnitude < 0.1f)
            {
                if (speed > 0)
                {
                    speed -= 1.0f;
                }
            }
        }
        else if (goingTowards == false)
        {
            if (dir.magnitude > 0.1f)
            {
                if (speed > 0)
                {
                    speed -= 1.0f;
                }
            }
        }
        
        

        //Adjust the speed to delta time
        curSpeed = speed * Time.deltaTime;


        //Apply obstacle avoidance
        dir += AvoidObstacles();
		dir.Normalize();

        //Rotate the vehicle to its target directional vector
        var rot = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, 20 *  Time.deltaTime);

        //Move the vehicle towards the target point
        transform.position += new Vector3(transform.forward.x , 0, transform.forward.z) * curSpeed;
    }
	
    //Calculate the new directional vector to avoid the obstacle
    public Vector3 AvoidObstacles()
    {
        RaycastHit hit;

        Ray ray = new Ray(transform.position, transform.forward);
        Debug.DrawRay(transform.position, transform.forward);

        Ray sideRay = new Ray(transform.position,transform.right);
        Debug.DrawRay(transform.position, transform.right);

        Ray sideRay2 = new Ray(transform.position, transform.right*-1);
        Debug.DrawRay(transform.position, transform.right*-1);

        if (Physics.Raycast(ray,out hit,minimumDistToAvoid,layer.value))
        {
            return hit.normal*speed * multiplier;
        }
        if (Physics.Raycast(sideRay, out hit, minimumDistToAvoid, layer.value))
        {
            return hit.normal * speed * multiplier;
        }
        if (Physics.Raycast(sideRay2, out hit, minimumDistToAvoid, layer.value))
        {
            return hit.normal * speed * multiplier;
        }
        else
        {
            return new Vector3(0.0f, 0.0f, 0.0f);
        }
	}
}