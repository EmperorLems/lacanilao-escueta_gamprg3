using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using JetBrains.Annotations;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class OLDUnitMovement : MonoBehaviour
{
    private NavMeshAgent agent;

    public List<Transform> waypoints;
    public float turnSpeed = 1440;
    

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.angularSpeed = turnSpeed;
    }

    public void MoveToWaypoint()
    {
        UpdateWaypoint();
        agent.isStopped = false;
        Vector3 pos = waypoints[0].position;
        pos.y = this.transform.position.y;
        agent.SetDestination(pos);
    }

    public void MoveToTarget(Vector3 location)
    {
        agent.isStopped = false;
        agent.SetDestination(location);
    }

    public void Stop()
    {
        agent.isStopped = true;
        agent.ResetPath();
    }

    public bool isTargetInFront(Vector3 targetLocation, float acceptableDistance)
    {
        if (Vector3.Distance(this.transform.position, targetLocation) > acceptableDistance)
        {
            return false;
        }
        
        Vector3 relativePos = targetLocation - transform.position;
        
        return Vector3.Dot(agent.transform.forward, relativePos) > 0.0f;
        
    }

    public void SetMovementSpeed(float speedVal) => agent.speed = speedVal / 20;

    public bool isStopped() => agent.isStopped;

    public bool isNearToWaypoint => Vector3.Distance(this.transform.position, waypoints[0].position) <= 10;

    public bool isNear(Vector3 location, float acceptableDistance) =>
        Vector3.Distance(this.transform.position, location) >= acceptableDistance;

    public void UpdateWaypoint()
    {
        if(waypoints[0] == null) waypoints.RemoveAt(0);
        else
        {
            if (isNearToWaypoint)
            {
                if(waypoints[0].GetComponent<Tower>() == null) waypoints.RemoveAt(0);
            }
        }
    }

    public void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed);
    }
}
