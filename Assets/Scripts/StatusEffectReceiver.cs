using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StatusEffectReceiver : MonoBehaviour
{
    private List<StatusEffect> statusEffects = new List<StatusEffect>();
    public List<StatusEffect> StatusEffects { get { return statusEffects; } }
    private GameObject container;
    public Action<StatusEffect, StatusEffectReceiver> OnApply;
    public Action<StatusEffect, StatusEffectReceiver> OnRemove;
    private void OnEnable()
    {
        container = new GameObject("StatusEffect Container");
        container.transform.parent = gameObject.transform;
        container.transform.localPosition = Vector3.zero;
    }

    public void ApplyStatusEffect(StatusEffect effect, GameObject source = null)
    {
        statusEffects.Add(effect);
        effect.transform.parent = container.transform;
        
        effect.Activate(this, source);
        OnApply?.Invoke(effect,this);
    }

    public void RemoveStatusEffect(StatusEffect effect)
    {
        statusEffects.Remove(effect);
        effect.Deactivate();
        OnRemove?.Invoke(effect,this);
    }

    public void RemoveStatusEffect(int effectID)
    {
        StatusEffect effect = statusEffects.Where(s => s.ID == effectID).FirstOrDefault();

        if (effect == null)
        {
            return;
        }
        RemoveStatusEffect(effect);
    }

    public bool isDuplicate(int effectID)
    {
        foreach (StatusEffect s in statusEffects)
        {
            if (effectID == s.ID)
            {
                return true;
            }
        }

        return false;
    }

    public bool isDuplicateByType<T>()
    {
        foreach(StatusEffect effect in statusEffects)
        {
            if (effect is T)
            {
                return true;
            }
        }
        return false;
    }

    public bool isDuplicateByType(Type t)
    {
        foreach(StatusEffect effect in statusEffects)
        {
            if (effect.GetType() == t)
            {
                return true;
            }
        }
        return false;
    }
    
    public bool isDuplicateByType(Type t, out StatusEffect effect)
    {
        foreach(StatusEffect e in statusEffects)
        {
            if (e.GetType() == t)
            {
                effect = e;
                return true;
            }
        }

        effect = null;
        return false;
    }
    public void RemoveEffectByType<T>()
    {
        foreach (StatusEffect effect in statusEffects)
        {
            if (effect is T)
            {
                RemoveStatusEffect(effect);
            }
        }
    }

    public bool isDuplicateByAbility<T>()
    {
        foreach (StatusEffect effect in statusEffects)
        {
            if (effect.Source.GetComponent<T>() != null)
            {
                Debug.LogWarning("Is Duplicate by Ability");
                return true;
            }
        }

        return false;
    }
    
    public void RemoveStatusEffectByType<T>() where T : StatusEffect
    {
        foreach(StatusEffect effect in statusEffects)
        {
            if (effect is T)
            {
                RemoveStatusEffect(effect);
            }
        }
    }
    
    public void DispelByType<T>() where T : StatusEffect
    {
        foreach(StatusEffect effect in statusEffects)
        {
            if (effect is T)
            {
                if (effect.isDispellable)
                {
                    RemoveStatusEffect(effect);
                }
            }
        }
    }
}
