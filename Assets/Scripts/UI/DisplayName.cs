using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DisplayName : MonoBehaviour
{
    public TMP_Text Name_TXT;
    public TMP_Text Faction_TXT;

    private void Start()
    {
        Hero hero = GetComponentInParent<Hero>();
        Name_TXT.text = hero.GetUnitData().name;
        Faction_TXT.text = hero.faction.ToString();
        Faction_TXT.color = hero.faction == Faction.Dire ? Color.red : Color.green;
    }
}
