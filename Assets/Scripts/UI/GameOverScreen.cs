using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] private Text resultText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setUp(bool playerWIns)
    {
        Time.timeScale = 0;
        GameObject[] UiObjects = GameObject.FindGameObjectsWithTag("PlayerUI");
        foreach (GameObject t in UiObjects)
        {
            t.SetActive(false);
        }

        if (playerWIns)
        {
            resultText.text = "Victory";
            resultText.color = Color.green;
        }
        else
        {
            resultText.text = "Defeat";
            resultText.color= Color.red;
        }
    }

    public void restartGame()
    {
        SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex ) ;
    }
}
