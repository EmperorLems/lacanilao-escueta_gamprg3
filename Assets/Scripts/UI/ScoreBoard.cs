using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

[System.Serializable]
class ScoreBar
{
    public ScoreManager score;
    public TMP_Text name_txt;
    public TMP_Text level_txt;
    public TMP_Text kill_txt;
    public TMP_Text assist_txt;
    public TMP_Text death_txt;
}
public class ScoreBoard : MonoBehaviour
{
    [SerializeField] private List<ScoreBar> direScoreBars  = new List<ScoreBar>(5);
    [SerializeField] private List<ScoreBar> radiantScoreBars = new List<ScoreBar>(5);
    void Start()
    {
        // ScoreManager[] scores = FindObjectsOfType<ScoreManager>();
        //
        // foreach (ScoreManager sc in scores)
        // {
        //     if (sc.hero.faction == Faction.Dire)
        //     {
        //     }
        // }
    }

    private void FixedUpdate()
    {
        foreach (ScoreBar sb in direScoreBars)
        {
            if (sb.score != null)
            {
                sb.name_txt.text = sb.score.heroName;
                sb.level_txt.text = sb.score.level.ToString();
                sb.kill_txt.text = sb.score.Kills.ToString();
                sb.assist_txt.text = sb.score.Assists.ToString();
                sb.death_txt.text = sb.score.Deaths.ToString();
            }
        }
        
        foreach (ScoreBar sb in radiantScoreBars)
        {
            if (sb.score != null)
            {
                sb.name_txt.text = sb.score.heroName;
                sb.level_txt.text = sb.score.level.ToString();
                sb.kill_txt.text = sb.score.Kills.ToString();
                sb.assist_txt.text = sb.score.Assists.ToString();
                sb.death_txt.text = sb.score.Deaths.ToString();
            }
        }
    }

    public void AddHeroToScoreBoard(Hero hero)
    {
        if (hero.faction == Faction.Dire)
        {
            foreach (ScoreBar sb in direScoreBars)
            {
                if (sb.score == null)
                {
                    sb.score = hero.GetComponent<ScoreManager>();
                    break;
                }
            }
        }
        else
        {
            foreach (ScoreBar sb in radiantScoreBars)
            {
                if (sb.score == null)
                {
                    sb.score = hero.GetComponent<ScoreManager>();
                    break;
                }
            }
        }
    }

}
