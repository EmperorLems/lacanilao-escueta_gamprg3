using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChannellingBar : MonoBehaviour
{
    [SerializeField] private Hero Hero;
    [SerializeField] private Image timer;
    [SerializeField] private GameObject bg;
    private AbilityUser _abilityUser;
    private void Start()
    {
        StartCoroutine(setup());
        Hero = GetComponentInParent<Hero>();
    }

    IEnumerator setup()
    {
        yield return new WaitForSeconds(.1f);
        _abilityUser = Hero.GetComponent<AbilityUser>();
        foreach (AbilityBase ability in _abilityUser.abilities)
        {
            if (ability.IsChanneled)
            {
                ability.OnChanneling.AddListener((ability =>
                {
                    StartCoroutine(updateBar(ability));
                } ));
            }
        }
    }

    IEnumerator updateBar(AbilityBase abilityBase)
    {
        bg.SetActive(true);
        while (Hero.state == UnitState.Channeling)
        {
            timer.fillAmount = abilityBase.ChannelledTimer / abilityBase.channelledDur;
            yield return new WaitForFixedUpdate();
        }
        bg.SetActive(false);
    }
}
