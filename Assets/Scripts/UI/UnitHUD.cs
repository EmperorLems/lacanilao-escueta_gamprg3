    using System.Collections;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

public class UnitHUD : MonoBehaviour
{
    [SerializeField] private Image hpBar; 
    [SerializeField] private Image mpBar;
    [SerializeField] private TMP_Text name_TXT;
    
    [SerializeField] private TMP_Text hp_TXT;
    [SerializeField] private TMP_Text mana_TXT;
    [SerializeField] private TMP_Text hpRegen_TXT;
    [SerializeField] private TMP_Text manaRegen_TXT;

    [SerializeField] private Image XpBar;
    [SerializeField] private TMP_Text level_TXT;
    [SerializeField] private TMP_Text xp_TXT;

    [SerializeField] private GameObject statsPanel;
    [SerializeField] private TMP_Text dmg_TXT;
    [SerializeField] private TMP_Text arm_TXT;
    [SerializeField] private TMP_Text spd_TXT;
    [SerializeField] private TMP_Text str_TXT;
    [SerializeField] private TMP_Text agi_TXT;
    [SerializeField] private TMP_Text int_TXT;
    
    [SerializeField] private TMP_Text Gold_TXT; 
    [SerializeField] private TMP_Text SkillPoint_TXT;

    [SerializeField] private Image unitIMG;
    [SerializeField] private StatScreenPopup statScreenPopup;
    [SerializeField] private effectIconHud _effectIconHud;
    private Sprite defaultSprite;
    private GameObject currentUnit;
    private Hero playerHero;
    private LevelingComponent playerLevel;
    private InventoryComponent playerInventory;
    void Start()
    {
        defaultSprite = unitIMG.sprite;
        playerHero = GameManager.instance.Player.GetComponent<Hero>();
        
        SetUnitInUI(playerHero.gameObject);
        
        name_TXT.text = playerHero.UnitName;
        
        playerInventory = playerHero.GetInventory();
        
        playerLevel = playerHero.GetLevelComponent();
        
        
    }
    
    void FixedUpdate()
    {
        SetPlayerUIBars();
        SetStatsHUD();
        SetHpAndManaTxtValues();
        SetLevelInfo();
        SetRegenValues();
    }
    
    public void SetPlayerUIBars()
    {
        currentUnit = (currentUnit == null) ? playerHero.gameObject : currentUnit;
        
        hpBar.fillAmount = currentUnit.GetComponentInChildren<HealthComponent>().GetHealthPercentage();
        
        if (mpBar.enabled)
        {
            if (currentUnit.GetComponent<Hero>() != null)
            {
                mpBar.fillAmount = currentUnit.GetComponent<Hero>().GetManaComponent().GetManaPercentage();
            }
            else mpBar.enabled = false;
        }
    }

    public void SetUnitInUI(GameObject obj)
    {
        // if obj is not null, set unitInUI as obj, else set it as player Hero
        currentUnit = (obj != null) ? obj : playerHero.gameObject;
        
        mpBar.enabled = mana_TXT.enabled = statScreenPopup.enabled = level_TXT.enabled = (currentUnit.layer == 10);

        statsPanel.SetActive(currentUnit.layer == 10);

        XpBar.enabled = xp_TXT.enabled = (currentUnit == playerHero.gameObject);

        hpRegen_TXT.enabled = manaRegen_TXT.enabled = (currentUnit.layer == 10);
        
        if (currentUnit.TryGetComponent(out Hero hero))
        {
            statScreenPopup.hero = hero;
        }

        if (currentUnit.TryGetComponent(out Unit unit))
        {
            unitIMG.sprite = unit.GetUnitData().icon;
        }
        else
        {
            unitIMG.sprite = defaultSprite;
        }

        if (currentUnit.TryGetComponent(out StatusEffectReceiver receiver))
        {
            _effectIconHud.SetNewReceiver(receiver);
        }

        name_TXT.text = (currentUnit.GetComponent<Unit>()!= null) ? currentUnit.GetComponent<Unit>().UnitName : currentUnit.name;

        Faction mFaction = (currentUnit.GetComponent<Tower>() != null) ? currentUnit.GetComponent<Tower>().faction : currentUnit.GetComponent<Unit>().faction;

        name_TXT.color = (mFaction == playerHero.faction) ? Color.green : Color.red;
    }

    private void SetStatsHUD()
    {
        if (currentUnit.layer == 10)
        {
            dmg_TXT.text = "DMG: " + playerHero.Damage;
            arm_TXT.text = "ARM: " + playerHero.TotalArmor;
            spd_TXT.text = "SPD: " + playerHero.AttackSpeed;
            str_TXT.text = "STR: " + (int)playerHero.GetHeroStats().str;
            agi_TXT.text = "AGI: " + (int)playerHero.GetHeroStats().agi;
            int_TXT.text = "INT: " + (int)playerHero.GetHeroStats().str;

        }
        else statsPanel.SetActive(false);
    }

    private void SetHpAndManaTxtValues()
    {
        hp_TXT.text = currentUnit.GetComponentInChildren<HealthComponent>().GetCurrentHealth().ToString("0") + 
                      "/" + currentUnit.GetComponentInChildren<HealthComponent>().GetMaxHealth().ToString("0");

        if (mana_TXT.enabled)
        {
            mana_TXT.text = "" + currentUnit.GetComponent<Hero>().CurrentMana.ToString("0") + 
                            "/" + currentUnit.GetComponent<Hero>().MaxMana.ToString("0") ;
        }
    }

    private void SetHpAndManaRegenTxt(float HpRegenValue, float ManaRegenValues)
    {
        if (hpRegen_TXT.enabled && manaRegen_TXT.enabled)
        {
            hpRegen_TXT.text = "+" + HpRegenValue.ToString("0.0");
            manaRegen_TXT.text = "+" + ManaRegenValues.ToString("0.0");
        }
    }
    private void SetRegenValues()
    {
        if (currentUnit.TryGetComponent(out Hero hero))
        {
            hpRegen_TXT.text = "+" + hero.HealthRegen.ToString("0.00");
            manaRegen_TXT.text = "+" + hero.ManaRegen.ToString("0.00");
        }
    }

    public void OnNotEnoughMana()
    {
        StartCoroutine(OnBlinkingMPBar());
    }
    
    IEnumerator OnBlinkingMPBar()
    {
        for (int i = 0; i < 2; i++)
        {
            mpBar.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            mpBar.color = Color.blue;
            yield return new WaitForSeconds(0.1f);
        }
        mpBar.color = Color.blue;
    }

    private void SetLevelInfo()
    {
        if (currentUnit.TryGetComponent(out Hero hero))
        {
            level_TXT.text = hero.GetLevelComponent().currentLevel.ToString();
            XpBar.fillAmount = (float)(hero.GetLevelComponent().currentXP - hero.GetLevelComponent().XpLevelFloor) / (float)(hero.GetLevelComponent().XPRequired - hero.GetLevelComponent().XpLevelFloor);
            xp_TXT.text = hero.GetLevelComponent().currentXP + "/" + playerLevel.XPRequired;
        }
        
        Gold_TXT.text = "GOLD: " + playerInventory.currentGold;
        SkillPoint_TXT.text = "SP: " + playerLevel.currentSkillPoints;
    }
}