using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class StatScreenPopup : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private GameObject statScreenPanel;
    [SerializeField] private TMP_Text atkspd_TXT;
    [SerializeField] private TMP_Text atkdmg_TXT;
    [SerializeField] private TMP_Text atkrng_TXT;
    [SerializeField] private TMP_Text movespd_TXT;
    [SerializeField] private TMP_Text manaregen_TXT;
    [SerializeField] private TMP_Text arm_TXT;
    [SerializeField] private TMP_Text magicresist_TXT;
    [SerializeField] private TMP_Text hpregen_TXT;
    [SerializeField] private TMP_Text STR_TXT;
    [SerializeField] private TMP_Text StrStat_TXT;
    [SerializeField] private TMP_Text AGI_TXT;
    [SerializeField] private TMP_Text AgiStat_TXT;
    [SerializeField] private TMP_Text INT_TXT;
    [SerializeField] private TMP_Text IntStat_TXT;

    private bool isMouseOver;
    private Hero mHero;

    public Hero hero
    {
        get => mHero;
        set => mHero = value;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isMouseOver)
        {
            setStatTxts();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isMouseOver = true;
        statScreenPanel.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isMouseOver = false;
        statScreenPanel.SetActive(false);
    }

    private void setStatTxts()
    {
        atkdmg_TXT.text = hero.Damage.ToString();
        arm_TXT.text = hero.TotalArmor.ToString();
        atkrng_TXT.text = hero.AttackRange.ToString();
        atkspd_TXT.text = hero.AttackSpeed.ToString();
        hpregen_TXT.text = hero.HealthRegen.ToString();
        magicresist_TXT.text = hero.MagicResist.ToString();
        manaregen_TXT.text = hero.ManaRegen.ToString();
        movespd_TXT.text = hero.MoveSpeed.ToString();

        STR_TXT.text = "STR: " + hero.GetHeroStats().str;
        StrStat_TXT.text = "HP: " + hero.GetHealthComponent().GetMaxHealth() + ", HP Regen" + hero.HealthRegen +
                           " and Magic Resist: " + hero.ManaRegen;
        
        AGI_TXT.text = "AGI: " + hero.GetHeroStats().agi;
        AgiStat_TXT.text = "Total Armor: " + hero.TotalArmor + ", Attack Speed: " +
                                              hero.AttackSpeed + " and Move speed: " + hero.MoveSpeed;

        INT_TXT.text = "INT: " + hero.GetHeroStats()._int;
        IntStat_TXT.text = "Mana: " + hero.GetManaComponent().GetMaxMana() + ", ManaRegen" + hero.ManaRegen;

        string damageTXT = " , DAMAGE: " + hero.Damage + " (Primary Role Bonus)";
        switch (hero.GetHeroStats().primary)
        {
                
            case 0:
                StrStat_TXT.text += damageTXT;
                break;
            case 1:
                AgiStat_TXT.text += damageTXT;
                break;
            case 2:
                IntStat_TXT.text += damageTXT;
                break;
        }
    }
}
