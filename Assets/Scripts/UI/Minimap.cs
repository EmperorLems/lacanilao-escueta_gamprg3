using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.EventSystems;

public class Minimap : MonoBehaviour
{
    [SerializeField] private Transform cam;
    [SerializeField] private RectTransform miniMapTransform;

    public float mapPosMinX = -250;
    public float mapPosMinY = -250;
    public float mapSize = 500;

    private Vector2 miniMapMax;
    private Vector2 miniMapMin;

    private bool isLeftMouseDown;
    private void Start()
    {
        Vector3[] corners = new Vector3[4];
        miniMapTransform.GetWorldCorners(corners);

        miniMapMax = corners[2];
        miniMapMin = corners[0];
        isLeftMouseDown = false;
    }

    public bool isOnMinimapClick(Vector2 mousePos)
    {
        if (miniMapMin.x <= mousePos.x && miniMapMin.y <= mousePos.y &&
            miniMapMax.x >= mousePos.x && miniMapMax.y >= mousePos.y)
        {
            return true;
        }
        
        return false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            isLeftMouseDown = true;
        }
        
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            isLeftMouseDown = false;
        }
    }

    private void FixedUpdate()
    {
        SetCameraPosition();
    }

    private void SetCameraPosition()
    {
        if (isLeftMouseDown)
        {
            Vector2 mousePos = Input.mousePosition;
            if (isOnMinimapClick(mousePos))
            {
                float newX = ((mousePos.x / miniMapMax.x) * mapSize) + mapPosMinX;
                float newZ = ((mousePos.y / miniMapMax.y) * mapSize) + mapPosMinY;

                Vector3 newPos = new Vector3(newX, cam.position.y, newZ);

                cam.position = newPos;
            }
        }
    }
}
