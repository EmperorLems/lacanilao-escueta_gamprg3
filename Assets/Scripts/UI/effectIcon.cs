using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class effectIcon : MonoBehaviour
{
    private StatusEffect effect;
    private StatusEffectReceiver _receiver;
    public Transform receiverContainer;
    public Transform hudContainer;
    [SerializeField] private Image timerIcon;
    [SerializeField] private GameObject timerBG;
    public Image iconImg;
    private RectTransform recTransform;

    public void Setup(StatusEffect mEffect, StatusEffectReceiver mReceiver)
    {
        effect = mEffect;
        _receiver = mReceiver;
        receiverContainer = _receiver.transform;
        recTransform = GetComponent<RectTransform>();
        if(!effect.hasDuration()) Destroy(timerBG);
    }

    public void CustomOnEnable()
    {
        gameObject.transform.SetParent(GameManager.instance.EffectIconHud.Container);
        //if(timerIcon!=null) timerIcon.gameObject.SetActive(effect.hasDuration());
        recTransform.rotation = Quaternion.Euler(Vector3.zero);
        recTransform.localScale = new Vector3(1, 1, 1);
        recTransform.sizeDelta = new Vector2(20, 20);
    }
    
    private void FixedUpdate()
    {
        if (timerBG!=null)
        {
            timerIcon.fillAmount = effect.Timer / effect.Duration;
        }
    }

    public void BackToOriginal()
    {
        gameObject.transform.SetParent(receiverContainer);
    }
}
