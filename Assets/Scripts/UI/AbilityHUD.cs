using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
struct AbilityBar
{
    public AbilityBase ability;
    public Button button;
    public Image Image;
    public TMP_Text controlText;
    public Button upgrade_BTN;
    public TMP_Text Level_TXT;
}
public class AbilityHUD : MonoBehaviour
{
    [SerializeField] AbilityBar[] _abilityBars = new AbilityBar[4];
    private AbilityUser player;
    

    void Start()
    {
        player = FindObjectOfType<PlayerController>().GetComponent<AbilityUser>();
        //CombatTesting();
        player.OnAddAbility.AddListener(AddAbilityIcons);
        
        if (player.TryGetComponent(out LevelingComponent lvlComp))
        {
            lvlComp.OnSkillPointChange.AddListener(UpgradeBtnVisibility);
        }
        
        foreach (AbilityBar i in _abilityBars)
        {
            i.button.enabled = true;
        }
    }

    void CombatTesting()
    {
        for (int i = 0; i < player.abilities.Length; i++)
        {
            AddAbilityIcons(player.abilities[i],i);
        }
    }
    public void AddAbilityIcons(AbilityBase ability, int index)
    {
        AbilityBar ab = _abilityBars[index];
        _abilityBars[index].ability = ability;
        ab.ability = ability;
        ab.Image.sprite = ability.Icon;
        
        ab.upgrade_BTN.onClick.AddListener(() =>
            OnUpgradeBtnClick(index));
        
        if (!ability.IsPassive)
        {
            ab.button.enabled = true;
            ab.controlText.color = Color.white;

            ability.OnChangeState.AddListener(state =>
            {
                OnAbilityChangeState(ab,state);
            });
            
            ab.button.onClick.AddListener(() =>
            {
                player.UseAbility(index);
            });
        }
        else
        {
            _abilityBars[index].controlText.color = Color.yellow;
            ab.button.enabled = false;
        }
        UpdateLevelText();
    }

    IEnumerator countdownUI(AbilityBar AB)
    {
        AB.controlText.color = AB.Image.color = Color.red;
        while (AB.ability.GetAbilityState == AbilityState.Cooldown)
        {
            AB.Image.fillAmount = AB.ability.CooldownTimer / AB.ability.CooldownDuration;
            yield return new WaitForEndOfFrame();
        }
        OnAbilityStandby(AB);
    }

    private void OnAbilityChangeState(AbilityBar ab, AbilityState state)
    {
        switch (state)
        {
            case AbilityState.Active:
                OnAbilityActive(ab);
                break;
            case AbilityState.Cooldown:
                StartCoroutine(countdownUI(ab));
                break;
            case AbilityState.Standby:
                OnAbilityStandby(ab);
                break;
        }
    }

    private void OnAbilityActive(AbilityBar ab)
    {
        ab.controlText.color = ab.Image.color = Color.green;
    }

    private void OnAbilityStandby(AbilityBar ab)
    {
        ab.controlText.color = ab.Image.color = Color.white;
    }

    private void UpgradeBtnVisibility(LevelingComponent lvlCOmp, int skillPoint)
    {
        if (skillPoint <= 0)
        {
            foreach (AbilityBar ab in _abilityBars)
            {
                ab.upgrade_BTN.gameObject.SetActive(false);
            }
        }
        if (skillPoint  > 0)
        {
            for (int j = 0; j < _abilityBars.Length; j++)
            {
                if (player.isAbilityUpgradable(j))
                {
                    _abilityBars[j].upgrade_BTN.gameObject.SetActive(true);
                }
                //else _abilityBars[j].upgrade_BTN.gameObject.SetActive(false);
            }
        }
    }

    public void OnUpgradeBtnClick(int index)
    {
        player.UpgradeAbility(index);
        UpdateLevelText();
    }

    private void UpdateLevelText()
    {
        foreach (AbilityBar ab in _abilityBars)
        {
            if (ab.ability != null)
            {
                ab.Level_TXT.text = ab.ability.Level.ToString();
            }
        }
    }
}
