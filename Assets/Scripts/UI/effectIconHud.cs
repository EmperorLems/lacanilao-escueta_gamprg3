using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;
using UnityEngine.UI;

public class effectIconHud : MonoBehaviour
{
    [SerializeField] private Transform container;
    public Transform Container => container;
    private StatusEffectReceiver receiver;

    public void SetNewReceiver(StatusEffectReceiver mReceiver)
    {
        if(mReceiver == receiver) return;
        
        if (receiver != null)
        {
            receiver.OnApply -= AddIcon;
            receiver.OnRemove -= RemoveIcon;
            foreach (StatusEffect effect in receiver.StatusEffects)
            {
                RemoveIcon(effect , mReceiver);
            }
        }

        receiver = mReceiver;
        foreach (StatusEffect effect in receiver.StatusEffects)
        {
            AddIcon(effect , mReceiver);
        }

        receiver.OnApply += AddIcon;
        receiver.OnRemove += RemoveIcon;
    }

    private void AddIcon(StatusEffect effect, StatusEffectReceiver mReceiver)
    {
        // if (mReceiver != receiver)
        // {
        //     Debug.Log("Not the same?? addIcon");
        // }

        if (effect.Icon != null)
        {
            effect.Icon.gameObject.SetActive(true);
            effect.Icon.CustomOnEnable();
        }
    }
    
    private void RemoveIcon(StatusEffect effect, StatusEffectReceiver mReceiver)
    {
        // if (mReceiver != receiver)
        // {
        //     Debug.Log("Not the same?? removeIcon");
        // }

        if (effect.Icon != null)
        {
            effect.Icon.BackToOriginal();
            effect.Icon.gameObject.SetActive(false);
        }
    }
}
