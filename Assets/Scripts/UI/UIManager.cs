using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Image timerImage;
    [SerializeField] private Text timerText;
    [SerializeField] private Text timeScale;


    private GameManager gameManager;
    private float gameTimer;


    void Start()
    {
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        gameManager.OnEnteredFiveMinutes += EnteredFiveMinutes;
    }

    // Update is called once per frame
    void Update()
    {
        SetTimerVisuals();
        timeScale.text = Time.timeScale.ToString() + ">>";
    }

    private void SetTimerVisuals()
    {
        float seconds = Mathf.FloorToInt(gameTimer % 60);
        float minutes = Mathf.FloorToInt(gameTimer / 60);

        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    public void SetTime(float mGameTime)
    {
        gameTimer = mGameTime;
    }
    
    
    private void EnteredFiveMinutes(bool mHasEnteredFiveMins)
    {
        if (mHasEnteredFiveMins == true)
        {
            if (gameManager.isDay == true)
            {
                timerImage.color = Color.yellow;
                gameManager.isDay = false;
            }
            else
            {
                timerImage.color = Color.blue;
                gameManager.isDay = true;
            }
        }
    }
}
