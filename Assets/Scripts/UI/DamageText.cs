using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageText : MonoBehaviour
{
    [SerializeField] private TMP_Text DMG_TXT;
    [SerializeField] private float fadeDuration = 1.0f;
    
    private float fadeStartTime;
    private Vector3 initialOffset, finalOffset;
    public Color startingColor,color_f;
    void Start()
    {
        fadeStartTime = Time.time;
        initialOffset = new Vector3(transform.position.x, transform.position.y + 10.0f, transform.position.z);
        finalOffset = new Vector3(initialOffset.x, initialOffset.y + 10.0f, initialOffset.z);
    }
    
    void Update()
    {
        float progress = (Time.time - fadeStartTime) / fadeDuration;

        if (progress <= 1)
        {
            transform.localPosition = Vector3.Lerp(initialOffset, finalOffset, progress);
            DMG_TXT.color = Color.Lerp(startingColor, color_f, progress);
        }
        else Destroy(this.gameObject);
    }

    public void SetText(float Damage)
    {
        DMG_TXT.text = Damage.ToString();
    }

    public void SetText(string message)
    {
        DMG_TXT.text = message;
    }
}
