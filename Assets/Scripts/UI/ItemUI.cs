using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    public List<Image> icons = new List<Image>();
    public Sprite DefaultSprite;
    private InventoryComponent inventory;

    private void Start()
    {
        inventory = FindObjectOfType<PlayerController>().GetComponent<InventoryComponent>();
        foreach (Image i in icons)
        {
            i.sprite = DefaultSprite;
            i.color = Color.black;
        }
        //inventory.OnChangeCount += UpdateImages;
    }

    void UpdateImages(List<Item> items)
    {
        for (int i = 0; i < icons.Count; i++)
        {
            if (items.Count < i + 1)
            {
                icons[i].sprite = DefaultSprite;
                icons[i].color = Color.black;
            }
            else
            {
                icons[i].sprite = items[i].icon;
                icons[i].color = Color.white;
            }
        }
    }
}
