using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct TopHudBar
{
    public Hero hero;
    public Image img;
    public TMP_Text txt;
}

public class TopHUD : MonoBehaviour
{
    public TopHudBar[] radiantBars = new TopHudBar[5];
    public TopHudBar[] direBars = new TopHudBar[5];
    public TMP_Text radScore_TXT, direScore_TXT;

    private UnitManager _unitManager;
    void Start()
    {
        _unitManager = GameManager.instance.getUnitManager();
        StartCoroutine(Setup());
    }

    private void FixedUpdate()
    {
        radScore_TXT.text = GameManager.instance.radiantScore.ToString();
        direScore_TXT.text = GameManager.instance.direScore.ToString();
    }

    IEnumerator Setup()
    {
        yield return new WaitForSeconds(0.1f);
        int i = 0;
        foreach (Hero mHero in _unitManager.RadiantHeroes)
        {
            radiantBars[i].hero = mHero;
            radiantBars[i].img.sprite = mHero.GetUnitData().icon;
            _unitManager.OnWaitingForRespawn += delegate(Hero hero) { StartCoroutine(OnToRespawn(hero)); };
            i++;
        }

        i = 0;
        foreach (Hero mHero in _unitManager.DireHeroes)
        {
            direBars[i].hero = mHero;
            direBars[i].img.sprite = mHero.GetUnitData().icon;
            _unitManager.OnWaitingForRespawn += delegate(Hero hero) { StartCoroutine(OnToRespawn(hero));  };
            i++;
        }
    }
    
    IEnumerator OnToRespawn(Hero hero)
    {
        Faction faction = hero.faction;
        List<TopHudBar> hudBar = (faction==Faction.Radiant) ? radiantBars.ToList() : direBars.ToList();
        foreach (TopHudBar thb in hudBar)
        {
            if (thb.hero == hero)
            {
                thb.txt.gameObject.SetActive(true);
                thb.img.color = Color.gray;
                while (hero.RespawnTimer > 0)
                {
                    thb.txt.text = hero.RespawnTimer.ToString();
                    yield return new WaitForFixedUpdate();
                }
                thb.txt.gameObject.SetActive(false);
                thb.img.color = Color.white;
            }
        }
    }
}
