using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Timeline;

public enum AttackType
{
    Basic,
    Pierce,
    Siege,
    Hero
}

public enum ArmorType
{
    Basic,
    Fortified,
    Hero
}

[System.Serializable]
public class CriticalStrike
{
    public CriticalStrike(float mChance, float mDamageModifier)
    {
        chance = mChance;
        damageModifier = mDamageModifier;
    }
    public float chance;
    public float damageModifier;
}

public class DamageReceiver : MonoBehaviour
{
    [SerializeField] private HealthComponent _healthComponent;

    private GameObject owner;
    private ArmorData armorData;
    [HideInInspector] public UnityEvent<GameObject> OnDamaged;

    public delegate float OnHit(DamageReceiver receiver, GameObject attacker, float dmg);

    public OnHit _onHit;

    public Action<GameObject> OnEvade;

    void Start()
    {
        _healthComponent = GetComponentInChildren<HealthComponent>();
        owner = this.gameObject;

        if(_healthComponent==null) Debug.LogWarning("WRONG!"); 
        
        armorData = (TryGetComponent(out Unit unit))
            ? unit.armorData
            : GetComponent<Tower>().TowerData.armorData;
        
        if(armorData==null) Debug.LogError("armor data is null");

        _onHit += (receiver, attacker, dmg) => dmg;
    }
    
    public bool TakeDamage(float dmg, AttackType attackerType, GameObject attacker, bool isMagical = false)
    {
        if (owner.TryGetComponent(out Unit unit))
        {
            if (unit.state == UnitState.Die) return false;
        }
        
        if(IsEvaded(attacker)) return false;

        if (isMagical) OnMagicalDamage(dmg, attacker);
        else OnPhysicalDmg(dmg, attackerType, attacker);
        return true;
    }

    public void DirectMaxHPDamage(float dmg, AttackType attackerType, GameObject attacker, bool isMagical = false)
    {
        float mod = (isMagical) ? CalculateMagicResist() : (GetDmgMultOnArmor() * GetDmgMultOnArmorType(attackerType));
        
        float newMaxHp = _healthComponent.GetMaxHealth() - (mod* dmg);
        
        //Debug.Log("raw : " + dmg + " // with mod: " + newMaxHp);
        
        _healthComponent.SetMaxHealth(newMaxHp);
    }
    
    private float GetDmgMultOnArmorType(AttackType attackerType)
    {
        switch (attackerType)
        {
            case AttackType.Basic:
                return armorData.basicDmgReceiveMult;
            case AttackType.Pierce:
                return armorData.pierceDmgReceiveMult;
            case AttackType.Siege:
                return armorData.siegeDmgReceiveMult;
            case AttackType.Hero:
                return armorData.heroDmgReceiveMult;
            default:
                return 1;
        }
    }
    public float GetDmgMultOnArmor()
    {
        if (owner.TryGetComponent(out Unit mUnit))
        {
            float dmgMultiplier = 1 - ((0.052f * mUnit.TotalArmor) / (0.9f + 0.048f * Math.Abs(mUnit.TotalArmor)));
            
            return dmgMultiplier;
        }
        else if (owner.TryGetComponent( out Tower tower))
        {
            float dmgMultiplier = 1 - ((0.052f * tower.Armor / (0.9f + 0.048f * Math.Abs(tower.Armor))));
        
            return dmgMultiplier;
        }

        return 1;
    }
    
    private float CalculateMagicResist()
    {
        if (owner.GetComponent<Tower>() != null)
        {
            return 0.5f;
        }
        else
        {
            return  1 - owner.GetComponent<Unit>().MagicResist;
        }
    }
    
    private void OnPhysicalDmg(float dmg, AttackType attackerAttackType, GameObject attacker)
    {
        bool isCritical  = IsCriticalHit(ref dmg ,attacker);
        dmg = _onHit(this, attacker, dmg);
        dmg *= GetDmgMultOnArmor();
        dmg *= GetDmgMultOnArmorType(attackerAttackType);
        DamageHP(dmg,attacker,isCritical);
    }

    private void OnMagicalDamage(float dmg, GameObject attacker)
    {
        DamageHP(dmg * CalculateMagicResist(), attacker);
    }

    private bool IsEvaded(GameObject attacker)
    {
        if (owner.TryGetComponent(out Unit unit))
        {
            float Rand = UnityEngine.Random.Range(0, float.MaxValue) / float.MaxValue;
            
            if (Rand < unit.Evasion)
            {
                OnEvade.Invoke(attacker);
                GameManager.instance.DamagePopup("MISS",transform.position, Color.blue);
                return true;
            }
        }

        return false;
    }

    private bool IsCriticalHit(ref float dmg, GameObject attacker)
    {
        if (attacker.TryGetComponent(out Unit unitAttacker))
        {
            if (unitAttacker.CriticalStrikes.Count == 0) return false;

            CriticalStrike csToApply = new CriticalStrike(0,0);
            
            foreach (CriticalStrike cs in unitAttacker.CriticalStrikes)
            {
                float Rand = UnityEngine.Random.Range(0, float.MaxValue) / float.MaxValue;

                if (Rand < cs.chance)
                {
                    if (cs.damageModifier > csToApply.damageModifier)
                    {
                        csToApply = cs;
                    }
                }
            }

            if (csToApply.damageModifier != 0)
            {
                dmg *= 1 + csToApply.damageModifier;
                return true;
            }
        }

        return false;
    }

    private void DamageHP(float dmg, GameObject attacker, bool isCritical =false)
    {
        dmg = (float)Math.Round(dmg, 1);
        
        _healthComponent.TakeDamage(dmg, attacker);
        
        Color color = (isCritical) ? Color.magenta : Color.red;
        
        float scale = (isCritical) ? 2 : 1;
        
        GameManager.instance.DamagePopup(dmg.ToString(),transform.position, color, scale);
        
        OnDamaged.Invoke(attacker);
    }

}
