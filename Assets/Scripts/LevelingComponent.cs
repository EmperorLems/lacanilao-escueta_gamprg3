using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelingComponent : MonoBehaviour
{
    [SerializeField] private LevelData lvlData;
    private LevelProperties currentLevelProperty;

    private Hero hero;
    
    private int level = 0;
    private int xp = 0;
    private int skillPoint = 0;
    
    
    [HideInInspector] public UnityEvent<LevelingComponent> OnBasicAbilityUpgradable;
    [HideInInspector] public UnityEvent<LevelingComponent> OnUltimateAbilityUpgradable;
    [HideInInspector] public UnityEvent<LevelingComponent, int> OnSkillPointChange;
    void Start()
    {
        level++;
        skillPoint++;

        if (isMaxLevel)
        {
            xp = XPRequired;
        }
        
        currentLevelProperty = lvlData.PropertiesInLevel[level];
        
        if(currentLevelProperty.isBasicAbilityUpgradable) OnBasicAbilityUpgradable.Invoke(this);
        if (currentLevelProperty.isUltimateAbilityUpgradable) OnUltimateAbilityUpgradable.Invoke(this);
        OnSkillPointChange.Invoke(this, skillPoint);
    }

    void OnLevelUp()
    {
        level++;
        skillPoint++;

        currentLevelProperty = lvlData.PropertiesInLevel[level];

        if (isMaxLevel)
        {
            xp = XPRequired;
        }

        hero = gameObject.GetComponent<Hero>();
        
        // raise stats
        hero.OnStatGrow();
        
        if(currentLevelProperty.isBasicAbilityUpgradable) OnBasicAbilityUpgradable.Invoke(this);
        if (currentLevelProperty.isUltimateAbilityUpgradable) OnUltimateAbilityUpgradable.Invoke(this);
        OnSkillPointChange.Invoke(this, skillPoint);
    }

    #region Getters and Setters

    public void AddXP(int xpVal)
    {
        if (isMaxLevel) return;
        
        xp+= xpVal;
        
        if (xp >= XPRequired)
        {
           OnLevelUp();
        }
    }

    public void UseSkillPoint()
    { 
        skillPoint--;
        OnSkillPointChange.Invoke(this, skillPoint);
    }
    
    public int XPRequired => currentLevelProperty.requiredXP;

    public int currentLevel => level;

    public int currentXP => xp;

    public bool isMaxLevel => level == (lvlData.PropertiesInLevel.Length - 1);

    public int KillXpReward => currentLevelProperty.killXP;

    public int currentSkillPoints => skillPoint;

    public int RespawnTime => currentLevelProperty.respawnTime;

    public int GoldLastHitBounty => currentLevelProperty.goldLastHitBounty;

    public int GoldAssistBounty => currentLevelProperty.goldAssistBounty;

    public int XpLevelFloor => lvlData.PropertiesInLevel[level - 1].requiredXP;

    #endregion

}
