using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public Hero hero => GetComponent<Hero>();
    private int kills = 0;
    private int assists = 0;
    private int deaths = 0;

    public int gold => hero.GetInventory().currentGold;

    public string heroName => hero.UnitName;
    public int level => hero.GetLevelComponent().currentLevel;
    
    public int Kills
    {
        get => kills;
        set => kills = value;
    }

    public int Assists
    {
        get => assists;
        set => assists = value;
    }

    public int Deaths
    {
        get => deaths;
        set => deaths = value;
    }
    
}
