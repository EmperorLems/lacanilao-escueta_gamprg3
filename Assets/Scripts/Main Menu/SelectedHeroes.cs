using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedHeroes
{
    private static readonly SelectedHeroes instance = new SelectedHeroes();
    
    public static List<Hero> DireHeroes = new List<Hero>();
    public static List<Hero> RadiantHeroes = new List<Hero>();
    public static Hero playerHero;
    
    private SelectedHeroes() {}
    
    public static SelectedHeroes Instance => instance;
}
