using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public List<Hero> Bots = new List<Hero>(), Players = new List<Hero>();

    [Header("Buttons")] 
    public Button V_BTN, C_BTN, N_BTN, P_BTN, S_BTN, Play_BTN;

    [Header("TEXT")] public TMP_Text heroName_TXT;
    private Faction choosenFaction = Faction.Radiant;
    private Hero currentHero = null;
    private void Awake()
    {
        C_BTN.onClick.AddListener(() =>SelectHero(Players[0]));
        N_BTN.onClick.AddListener(() =>SelectHero(Players[1]));
        P_BTN.onClick.AddListener(() =>SelectHero(Players[2]));
        S_BTN.onClick.AddListener(() =>SelectHero(Players[3]));
        V_BTN.onClick.AddListener(() =>SelectHero(Players[4]));
        
        Play_BTN.onClick.AddListener(OnPlay);
    }

    private void SelectHero(Hero hero)
    {
        currentHero = hero;
        Play_BTN.gameObject.SetActive(true);
        heroName_TXT.gameObject.SetActive(true);
        heroName_TXT.text = currentHero.GetUnitData().name;
    }

    void OnPlay()
    {
        if(currentHero==null) Debug.Log("hero is null");
        SetPlayer();
        SetBots();
        SceneManager.LoadScene("GameScene");
    }

    void SetPlayer()
    {
        SelectedHeroes.playerHero = currentHero;
    }

    void SetBots()
    {
        List<Hero> direHeroes = new List<Hero>();
        List<Hero> radiantHeroes = new List<Hero>();
        direHeroes.AddRange(Bots);
        radiantHeroes.AddRange(Bots);
        foreach (Hero h in direHeroes)
        {
            if (h.GetUnitData().name == currentHero.GetUnitData().name &&
                choosenFaction == Faction.Dire)
            {
                direHeroes.Remove(h);
                break;
            }
        }
        foreach (Hero h in radiantHeroes)
        {
            if (h.GetUnitData().name == currentHero.GetUnitData().name &&
                choosenFaction == Faction.Radiant)
            {
                radiantHeroes.Remove(h);
                break;
            }
        }
        
        SelectedHeroes.DireHeroes = direHeroes;
        SelectedHeroes.RadiantHeroes = radiantHeroes;
    }
    
    public void Quit()
    {
        
    }
}
