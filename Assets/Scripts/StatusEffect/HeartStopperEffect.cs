using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartStopperEffect : StatusEffect
{
    public float MaxHpPercentLost;
    public Action<bool> OnNecrophosKills;
    public GameObject Necrophos;
    
    private DamageReceiver dmgReceiver;
    private HealthComponent hpComponent;
    protected override void OnActivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            dmgReceiver = unit.GetComponent<DamageReceiver>();
            hpComponent = unit.GetHealthComponent();

            unit.OnUnitDeath.AddListener((unit, attacker) =>
            {
                if (attacker == Necrophos)
                {
                    bool isHero = unit is Hero;
                    OnNecrophosKills.Invoke(isHero);
                }
            });
        }
    }

    protected override void OnTIck()
    {
        float MaxHpLost = (MaxHpPercentLost * hpComponent.GetMaxHealth()) * tick;
        dmgReceiver.DirectMaxHPDamage(MaxHpLost, AttackType.Hero, Source, true);
        //Debug.Log("lost" + MaxHpLost + " ==== newMaxHP: " + hpComponent.GetMaxHealth());
    }
}
