using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunStatusEffect : StatusEffect
{
    private Unit unit;
    protected override void OnActivate()
    {
        unit = Target.GetComponent<Unit>();
    }

    private void LateUpdate()
    {
        if(unit.state == UnitState.Die) return;
        unit.state = UnitState.Idle;
    }

    protected override void OnDeactivate()
    {
        if(unit.state == UnitState.Die) return;
        unit.state = UnitState.Chase;
    }
}
