using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StatusEffect : MonoBehaviour
{
    public int ID;
    public float Duration = 0;
    public bool isStackable;
    protected bool isActivated;
    protected float timer;
    public bool isDispellable;
    public bool removeOnDeath = true;
    public UnityEvent<StatusEffect, StatusEffectReceiver> OnEffectEnd;

    public StatusEffectReceiver Target { get; private set; }
    public GameObject Source { get; private set; }
    public effectIcon Icon = null;
    public float tick = 0.1f;

    protected virtual void OnActivate() {}
    protected virtual void OnDeactivate() {}

    public void Activate(StatusEffectReceiver target, GameObject source = null)
    {
        if (isActivated) return;
        Target = target;
        Source = source;
        isActivated = true;
        timer = Duration;
        if (Icon!= null)
        {
            //Icon.gameObject.SetActive(false);
            Icon.Setup(this, Target);
        }
        OnActivate();
        if (hasDuration())
        {
            StartCoroutine(EveryTick(tick));
        }
    }

    public void Deactivate()
    {
        isActivated = false;
        OnDeactivate();
        if (Icon != null)
        {
            Destroy(Icon.gameObject);
        }

        if (Target.TryGetComponent(out Unit unit))
        {
            if (unit.state != UnitState.Die)
            {
                OnEffectEnd.Invoke(this, Target);
            }
        }
        Destroy(this.gameObject);
    }
    
    public void RefreshStatusEffect() { timer = 0; }

    protected virtual void OnTIck()
    {
        
    }

    protected IEnumerator EveryTick(float tick)
    {
        while (timer > 0 && Target.gameObject.activeSelf)
        {
            yield return new WaitForSeconds(tick);
            OnTIck();
            timer-= tick;
        }
        RemoveFromReceiver();
    }

    public void RemoveFromReceiver()
    {
        Target.RemoveStatusEffect(this);
    }

    public bool IsActivated => isActivated;
    public float Timer => timer;

    public bool hasDuration()
    {
        return Duration > 0;
    }
}
