using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvasionBonusEffect : StatusEffect
{
    private float evasion = 0;

    protected override void OnActivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            UpdateEvasion(evasion);
        }
        else RemoveFromReceiver();
    }

    protected override void OnDeactivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.RemoveEvasionStack(evasion);
        }
    }

    public void UpdateEvasion(float val)
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.RemoveEvasionStack(evasion);
            evasion = val;
            unit.StackEvasion(evasion);
        }
    }
}
