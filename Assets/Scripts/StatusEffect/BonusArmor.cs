using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class BonusArmor : StatusEffect
{
    public float armorBonus;
    
    protected override void OnActivate()
    {
        // //Debug.LogWarning("activated Target: " + Target + "Source: " + Source);
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.AddBonusArmor(armorBonus);
        }
    }

    protected override void OnDeactivate()
    {
        // //Debug.LogWarning("deactivated Target: " + Target + "Source: " + Source);
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.AddBonusArmor(-armorBonus);
        }
    }
}
