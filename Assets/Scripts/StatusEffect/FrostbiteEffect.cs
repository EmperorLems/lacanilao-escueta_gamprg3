using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostbiteEffect : StatusEffect
{
    public float TotalDamage;
    private Unit unit;
    private DamageReceiver dmgReceiver;
    protected override void OnActivate()
    {
        unit = Target.GetComponent<Unit>();
        dmgReceiver = unit.GetComponent<DamageReceiver>();
    }

    private void LateUpdate()
    {
        if(unit.state == UnitState.Die) return;
        unit.state = UnitState.Idle;
    }

    protected override void OnDeactivate()
    {
        if(unit.state == UnitState.Die) return;
        unit.state = UnitState.Chase;
    }

    protected override void OnTIck()
    {
        dmgReceiver.TakeDamage(TotalDamage * tick, AttackType.Hero, Source, true);
    }
}
