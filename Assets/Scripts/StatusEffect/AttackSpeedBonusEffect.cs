using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackSpeedBonusEffect : StatusEffect
{
    public float speedMod;

    protected override void OnActivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.AttackSpeed += speedMod;
            Icon.iconImg.color = (speedMod > 0) ? new Color(134,255,144) : new Color(253,82,91);
            // StartCoroutine(EveryTick(0.1f));
        }
        else RemoveFromReceiver();
    }

    protected override void OnDeactivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.AttackSpeed -= speedMod;
        }
    }
}
