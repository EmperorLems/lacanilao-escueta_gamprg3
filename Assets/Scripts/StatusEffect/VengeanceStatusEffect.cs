using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VengeanceStatusEffect : StatusEffect
{
    public float rangeMultBonus;
    public float damageMultBonus;
    
    protected override void OnActivate()
    {
        Creeps c = Target.GetComponent<Creeps>();

        c.GetComponent<TargetsAcquisition>().radius *= (1 + rangeMultBonus);
        
        float newdmg = c.Damage * (1 + damageMultBonus);
        c.Damage = newdmg;
    }

    protected override void OnDeactivate()
    {
        Creeps c = Target.GetComponent<Creeps>();
        
        c.GetComponent<TargetsAcquisition>().radius /= (1 + rangeMultBonus);
        
        float oldDmg = c.Damage / (1 + damageMultBonus);
        c.Damage = oldDmg;
    }
}
