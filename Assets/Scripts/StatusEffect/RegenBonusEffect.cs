using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenBonusEffect : StatusEffect
{
    public float HpRegenBonus = 1;
    public float ManaRegenBonus = 1;
    protected override void OnActivate()
    {
        if (Target.TryGetComponent(out Hero hero))
        {
            hero.HealthRegen += HpRegenBonus;
            hero.ManaRegen += ManaRegenBonus;
        }
        else RemoveFromReceiver();
    }

    protected override void OnDeactivate()
    {
        if (Target.TryGetComponent(out Hero hero))
        {
            hero.HealthRegen -= HpRegenBonus;
            hero.ManaRegen -= ManaRegenBonus;
        }
    }

    public void UpdateBonus(float hpRegen, float mpRegen)
    {
        if (Target.TryGetComponent(out Hero hero))
        {
            hero.HealthRegen -= HpRegenBonus;
            hero.ManaRegen -= ManaRegenBonus;

            HpRegenBonus = hpRegen;
            ManaRegenBonus = mpRegen;

            OnActivate();
        }
    }
}
