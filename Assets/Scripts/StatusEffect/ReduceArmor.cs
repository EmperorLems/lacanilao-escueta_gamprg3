using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReduceArmor : StatusEffect
{
    public float reduceVal;
    
    protected override void OnActivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.AddBonusArmor(-reduceVal);
        }
        else
        {
            Target.RemoveStatusEffect(this);
        }
    }

    protected override void OnDeactivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.AddBonusArmor(reduceVal);
        }
        
        Destroy(this.gameObject);
    }
    
}
