using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalImmuneEffect : StatusEffect
{
    private LayerMask prevLayer;
    protected override void OnActivate()
    {
        prevLayer = Target.gameObject.layer;
        Target.gameObject.layer = GameManager.instance.ignoreRaycastLayer;
    }

    protected override void OnDeactivate()
    {
        Target.gameObject.layer = prevLayer;
    }
}
