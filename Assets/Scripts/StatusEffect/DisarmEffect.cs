using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisarmEffect : StatusEffect
{
    private Unit unitTarget;
    protected override void OnActivate()
    {

        if (Target.TryGetComponent(out Unit unit))
        {
            unitTarget = unit;
        }
    }

    private void LateUpdate()
    {
        if(unitTarget == null) return;
        
        if (unitTarget.state == UnitState.Attack)
        {
            unitTarget.state = UnitState.Chase;
        }
    }
}
