using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusDamageEffect : StatusEffect
{
    public float bonusDmg; 
    protected override void OnActivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.Damage += bonusDmg;
        }
    }

    protected override void OnDeactivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.Damage -= bonusDmg;
        }
    }
}
