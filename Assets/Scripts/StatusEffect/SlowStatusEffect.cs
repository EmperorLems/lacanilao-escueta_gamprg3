using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowStatusEffect : StatusEffect
{
    public float speedMod;

    protected override void OnActivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.MoveSpeed *= speedMod;
            Icon.iconImg.color = (speedMod > 1) ? new Color(134,255,144) : new Color(253,82,91);
        }
        else RemoveFromReceiver();
    }

    protected override void OnDeactivate()
    {
        if (Target.TryGetComponent(out Unit unit))
        {
            unit.MoveSpeed /= speedMod;
        }
    }
}
