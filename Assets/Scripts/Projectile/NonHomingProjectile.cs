using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class NonHomingProjectile : ProjectileScript
{
    private bool isDestroyAfterHit;
    private Vector3 startingPos;
    private float distanceToDestroy;
    private Faction factionToExclude;
    private List<GameObject> objectsHit = new List<GameObject>();
    protected override void Movement()
    {
        transform.Translate(speed * Time.deltaTime * Vector3.forward);
        
        if(Vector3.Distance(startingPos,transform.position) >= distanceToDestroy) Destroy(gameObject);
    }

    protected override void OnTrigger(GameObject obj)
    {
        if (AddObjectToList(obj))
        {
            Faction faction = factionToExclude;

            if (obj.TryGetComponent(out Unit unit)) faction = unit.faction;
            else if (obj.TryGetComponent(out Tower tower)) faction = tower.faction;
            
            if(faction!= factionToExclude) DamageHit(obj);
            
            OnHit.Invoke(obj);
            if(isDestroyAfterHit) Destroy(this.gameObject);
        }
    }
    
    public void Setup(float damage, float distance, GameObject mAttacker, Faction mfactionToExclude, AttackType AT, float mSpeed,
        bool DestroyAfterHit = true, bool isMagical = false)
    {
        projectileDamage = damage;
        distanceToDestroy = distance;
        startingPos = transform.position;
        factionToExclude = mfactionToExclude;
        attacker = mAttacker;
        attackType = AT;
        isDestroyAfterHit = DestroyAfterHit;
        speed = mSpeed;
        isMagicDMG = isMagical;
        
        StartCoroutine(DestroyAfterLifeSpan());
    }
    
    bool AddObjectToList(GameObject obj)
    {
        if (objectsHit.Contains(obj))
        {
            return false;
        }
        else
        {
            objectsHit.Add(obj);
            return true;
        }
    }
}
