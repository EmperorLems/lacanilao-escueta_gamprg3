using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : ProjectileScript
{
    private Vector3 startingPos;
    private Vector3 scaleTarget;
    private Faction factionToExclude;
    private Vector3 scaleSpeed;
    private List<GameObject> objectsHit = new List<GameObject>();
    protected override void Movement()
    {
        if (transform.localScale.x <= scaleTarget.x)
        {
            transform.localScale += scaleSpeed;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    protected override void OnTrigger(GameObject obj)
    {
        if (AddObjectToList(obj))
        {
            Faction faction = factionToExclude;

            if (obj.TryGetComponent(out Unit unit)) faction = unit.faction;
            else if (obj.TryGetComponent(out Tower tower)) faction = tower.faction;
            
            OnHit.Invoke(obj);
        }
    }
    
    public void Setup(Vector3 pos, Vector3 scale, float mDuration = 1f)
    {
        transform.position = pos;
        scaleTarget = scale;
        scaleSpeed = new Vector3(
            scaleTarget.x / (50 * mDuration), 
            scaleTarget.y / (50 * mDuration), 
            scaleTarget.z / (50 * mDuration)
            );
        StartCoroutine(DestroyAfterLifeSpan());
    }
    
    bool AddObjectToList(GameObject obj)
    {
        if (objectsHit.Contains(obj))
        {
            return false;
        }
        else
        {
            objectsHit.Add(obj);
            return true;
        }
    }
}
