using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingProjectile : ProjectileScript
{
    private GameObject projectileTarget;

    protected override void Movement()
    {
        if (projectileTarget == null || !projectileTarget.activeSelf)
        {
            Destroy(this.gameObject);
        }
        
        this.transform.LookAt(projectileTarget.transform);
        transform.Translate(speed * Time.deltaTime * Vector3.forward);
    }
    
    protected override void OnTrigger(GameObject obj)
    {
        if (projectileTarget == obj)
        {
            if (canDamage)
            {
                DamageHit(obj);
            }
            OnHit.Invoke(obj);
            
            Destroy(this.gameObject);
        }
    }
    
    public void Setup(GameObject target, float damage, GameObject mAttacker, AttackType AT, bool isMagical = false, float life = 10.0f, float mSpeed = 500)
    {
        projectileTarget = target;
        projectileDamage = damage;
        attacker = mAttacker;
        attackType = AT;
        isMagicDMG = isMagical;
        lifeSpan = life;
        speed = mSpeed / 10;
        canDamage = true;
    }

    //Use this if not damaging the target
    public void Setup(GameObject target, float life = 10f, float mSpeed = 500)
    {
        projectileTarget = target;
        lifeSpan = life;
        speed = mSpeed / 10;
        canDamage = false;
    }
}
