using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class ProjectileScript : MonoBehaviour
{
    protected GameObject attacker;
    protected AttackType attackType;
    protected float projectileDamage;
    protected bool isMagicDMG = false;
    protected float speed = 50;
    protected float lifeSpan = 10;
    protected bool canDamage = true;
    public UnityEvent<GameObject> OnHit;
    
    private void FixedUpdate()
    {
        Movement();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == attacker) return;
        
        if (GameManager.instance.isInUnitsAndTowerLayer(other.gameObject))
        {
            OnTrigger(other.gameObject); 
        }
        // else if (other.gameObject.layer == 8 || other.gameObject.layer == 9)
        // {
        //     OnTrigger(other.gameObject);
        // }
        
    }

    protected IEnumerator DestroyAfterLifeSpan()
    {
        yield return new WaitForSeconds(lifeSpan);
        Destroy(this.gameObject);
    }

    protected void DamageHit(GameObject obj)
    {
        DamageReceiver dmgReceiver = obj.GetComponent<DamageReceiver>();
        
        if(!dmgReceiver) return;
        
        dmgReceiver.TakeDamage(projectileDamage, attackType, attacker, isMagicDMG);
    }
    
    protected virtual void Movement() { }

    protected virtual void OnTrigger(GameObject obj) { }
}
