using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugScript : MonoBehaviour
{
    [SerializeField] private Button Destroy_BTN;
    [SerializeField] private Button Kill_BTN;
    [SerializeField] private Button levelup_BTN;
    [SerializeField] private PlayerController player;
    
    private DamageReceiver currentTower;
    private DamageReceiver currentUnit;
    private Hero playerHero;
    
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        player.OnLeftClickEvent.AddListener(LeftClick);
        playerHero = player.GetComponent<Hero>();
    }
    

    public void DamageTower()
    {
        if (currentTower == null)
        {
            Destroy_BTN.gameObject.SetActive(false);
            return;
        }
        currentTower.TakeDamage(999999,AttackType.Hero,playerHero.gameObject);
        currentTower = null;
        Destroy_BTN.gameObject.SetActive(false);
        
    }

    public void KillUnit()
    {
        if (currentUnit == null)
        {
            Kill_BTN.gameObject.SetActive(false);
            return;
        }
        
        currentUnit.TakeDamage(9999999,AttackType.Hero,playerHero.gameObject);
        currentUnit = null;
        Destroy_BTN.gameObject.SetActive(false);
    }

    private void LeftClick(GameObject obj, Vector3 location)
    {
        if (obj.layer == 8 || obj.layer == 9)
        {
            Destroy_BTN.gameObject.SetActive(true);
            currentTower = obj.GetComponent<DamageReceiver>();
            
            Kill_BTN.gameObject.SetActive(false);
            currentUnit = null;
            
            levelup_BTN.gameObject.SetActive(false);
        }
        else if(obj.gameObject.GetComponent<Unit>() != null)
        {
            currentTower = null;
            Destroy_BTN.gameObject.SetActive(false);
            
            Kill_BTN.gameObject.SetActive(true);
            currentUnit = obj.GetComponent<DamageReceiver>();

            if (obj.gameObject.GetComponent<Hero>() != null)
            {
                levelup_BTN.gameObject.SetActive(true);
            }
            else levelup_BTN.gameObject.SetActive(false);
        }
    }
    
    public void SpeedTime()
    {
        Time.timeScale *= 2;
        if (Time.timeScale > 8)
        {
            Time.timeScale = 1;
        }
    }

    public void RevertTimeToNormal()
    {
        Time.timeScale = 1;
    }

    public void ResetPlayerMana()
    {
        playerHero.GetManaComponent().AddMana(99999999);
    }

    public void ResetPlayerHP()
    {
        playerHero.GetHealthComponent().HealHP(999999);
    }

    public void ResetAbilityCooldown()
    {
        AbilityUser user = playerHero.GetComponent<AbilityUser>();
        foreach (AbilityBase a in user.abilities)
        {
            a.CooldownTimer = 99999;
        }
    }

    public void LevelupHero()
    {
        LevelingComponent lvlComp = player.GetComponent<LevelingComponent>();
        if (lvlComp == null) return;
        
        lvlComp.AddXP(lvlComp.XPRequired - lvlComp.currentXP);
    }

    public void Motherlode()
    {
        player.GetComponent<InventoryComponent>().AddGold(999999);
    }

    public void DebugAddGold()
    {
        playerHero.GetInventory().DebugAddGold();
    }
}
