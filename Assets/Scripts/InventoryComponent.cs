using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryComponent : MonoBehaviour
{
    [SerializeField] private int gold = 600;
    private List<Item> items = new List<Item>(6);
    public Action<List<Item>> OnChangeCount;

    private InventoryController inventoryController;
    #region Getters and Setters
    public List<Item> Items { get => items; }

    public int currentGold { get => gold; }
    
    public void subtractGold(int GoldToSubtract) { gold -= GoldToSubtract; }
    public void AddGold(int GoldToAdd) { gold += GoldToAdd; }

    #endregion

    private void Awake()
    {
        inventoryController = GameObject.Find("InventoryManager").GetComponent<InventoryController>();
    }

    public void UseItem(int index)
    {
        if(items.Count < index+1) return;
        if (items[index].GetComponent<BasicItem>() != null) return;
        Items[index].UseItem();
    }

    public void AddItem(Item item)
    {
        //if (items.Count < 6)
        //{
        //    item.Inventory = this;
        //    items.Add(item);
        //    OnChangeCount?.Invoke(Items);
        //}
        inventoryController.AddItem(item);
        if (item.GetComponent<BasicItem>() != null)
        {
            item.UseItem();
        }
    }

    public void RemoveItem(Item item)
    {
        //items.Remove(item);
        //OnChangeCount?.Invoke(Items);
        Destroy(item.gameObject);
    }

    public void DebugAddGold()
    {
        gold += 999999;
    }
}
