using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public List<ItemSlots> itemSlots;
    // //private BuyItem buyListener = new BuyItem();
    //
    // private void Awake()
    // {
    //     buyListener.OnItemBought += AddItem;
    // }

    private void Update()
    {
        Alpha1();
        Alpha2();
        Alpha3();
        Alpha4();
        Alpha5();
        Alpha6();
    }

    public void AddItem(Item itemBought)
    {
        foreach (ItemSlots slot in itemSlots)
        {
            if (slot.hasItem == false)
            {
                Item addedItem = Instantiate(itemBought, slot.transform.position, Quaternion.identity);
                addedItem.transform.parent = slot.transform;
                addedItem.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                return;
            }
        }
    }

    #region Controls

    private void Alpha1()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (itemSlots[0].hasItem)
            {
                if (itemSlots[0].item.GetComponent<Consumables>())
                {
                    itemSlots[0].SetRecieverForConsumables(itemSlots[0].item.GetComponent<Consumables>());
                }
                UseItem(itemSlots[0].item);
            }
        }
    }

    private void Alpha2()
    {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (itemSlots[1].hasItem)
            {
                if (itemSlots[1].item.GetComponent<Consumables>())
                {
                    itemSlots[1].SetRecieverForConsumables(itemSlots[1].item.GetComponent<Consumables>());
                }
                UseItem(itemSlots[1].item);
            }
        }
    }

    private void Alpha3()
    {
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (itemSlots[2].hasItem)
            {
                if (itemSlots[2].item.GetComponent<Consumables>())
                {
                    itemSlots[2].SetRecieverForConsumables(itemSlots[2].item.GetComponent<Consumables>());
                }
                UseItem(itemSlots[2].item);
            }
        }
    }
    private void Alpha4()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (itemSlots[3].hasItem)
            {
                if (itemSlots[3].item.GetComponent<Consumables>())
                {
                    itemSlots[3].SetRecieverForConsumables(itemSlots[3].item.GetComponent<Consumables>());
                }
                UseItem(itemSlots[3].item);
            }
        }
    }

    private void Alpha5()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (itemSlots[4].hasItem)
            {
                if (itemSlots[4].item.GetComponent<Consumables>())
                {
                    itemSlots[4].SetRecieverForConsumables(itemSlots[4].item.GetComponent<Consumables>());
                }
                UseItem(itemSlots[4].item);
            }
        }
    }

    private void Alpha6()
    {
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            if (itemSlots[5].hasItem)
            {
                if (itemSlots[5].item.GetComponent<Consumables>())
                {
                    itemSlots[5].SetRecieverForConsumables(itemSlots[5].item.GetComponent<Consumables>());
                }
                UseItem(itemSlots[5].item);
            }
        }
    }

    #endregion
    private void UseItem(Item item)
    {
        item.UseItem();
    }
}
