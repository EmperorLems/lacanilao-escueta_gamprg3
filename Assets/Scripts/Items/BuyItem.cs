using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class BuyItem : MonoBehaviour, IPointerClickHandler
{
    [Header("Item Costs")]
    [SerializeField] private int itemCost;

    [SerializeField] private Item itemPrefab;
    private InventoryComponent inventory;
    public Action<Item> OnItemBought;

    private void Start()
    {
        inventory = FindObjectOfType<PlayerController>().GetComponent<InventoryComponent>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (this.transform.gameObject.layer == 11)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                if (inventory.currentGold >= itemPrefab.cost)
                {
                    inventory.subtractGold(itemCost);
                    Item item = Instantiate(itemPrefab, inventory.transform);
                    item.hero = inventory.GetComponent<Hero>();
                    inventory.AddItem(item);
                }
            }
        }
    }
    
}
