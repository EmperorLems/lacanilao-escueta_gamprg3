using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopButton : MonoBehaviour
{
    [SerializeField] private GameObject itemShop;

    public void OnPressed()
    {
        if (itemShop.gameObject.activeSelf == false)
        {
            itemShop.SetActive(true);
        }
        else
        {
            itemShop.SetActive(false);
        }
    }
}
