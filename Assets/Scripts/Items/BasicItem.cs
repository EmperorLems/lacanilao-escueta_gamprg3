using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

enum AdditionalStats { movespeed, armor, attackSpeed, lifeSteal, hpRegen, manaRegen, magicResist, attackdmg, evasion, heroStats }
public class BasicItem : Item
{
    [SerializeField] private AdditionalStats _additionalStats;
    [SerializeField] private float value;
    public override void UseItem()
    {
        switch (_additionalStats)
        {
            case AdditionalStats.movespeed: MoveSpeed();
                break;
            case AdditionalStats.armor: Armor();
                break;
            case AdditionalStats.attackSpeed: AttackSpeed();
                break;
            case AdditionalStats.lifeSteal: LifeSteal();
                break;
            case AdditionalStats.hpRegen: HPRegen();
                break;
            case AdditionalStats.manaRegen: manaRegen();
                break;
            case AdditionalStats.magicResist: MagicResist();
                break;
            case AdditionalStats.attackdmg: AttackDamage();
                break;
            case AdditionalStats.evasion: evasion();
                break;
            case AdditionalStats.heroStats: stats();
                break;
        }
    }

    void MoveSpeed()
    {
        hero.MoveSpeed += value;
    }

    void Armor()
    {
        hero.Armor += value;
    }

    void AttackSpeed()
    {
        hero.AttackSpeed += value;
    }

    void LifeSteal()
    {
        
    }
    
    void HPRegen() { hero.HealthRegen+= value; }
    void manaRegen() { hero.ManaRegen+= value; }
    void MagicResist() { hero.StackMagicResist(value); }
    void AttackDamage() { hero.Damage+= value; }
    void evasion() { hero.StackEvasion(value); }

    void stats()
    {
        HeroStats stats = new HeroStats();
        stats._int = value;
        stats.agi = value;
        stats.str = value;
        hero.AddHeroStats(stats);
    }
}
