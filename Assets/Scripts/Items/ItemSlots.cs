using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ItemSlots : MonoBehaviour, IDropHandler
{
    public bool hasItem;
    public Item item;

    Consumables consumeListener;

    private void Update()
    {
        if (this.transform.childCount > 0)
        {
            hasItem = true;
            item = this.GetComponentInChildren<Item>();
        }
        else
        {
            item = null;
            hasItem = false;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            DragHandler itemDragged = eventData.pointerDrag.GetComponent<DragHandler>();
            if (item != null)
            {
                GameObject tempPosition = itemDragged.transform.parent.gameObject;

                //Swap Position
                itemDragged.ChangeDefaultPos(this.transform);
                itemDragged.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;

                item.GetComponent<DragHandler>().ChangeDefaultPos(tempPosition.transform);
                item.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            }
            else
            {
                item = itemDragged.GetComponent<Item>();
                itemDragged.ChangeDefaultPos(this.transform);
                itemDragged.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            }
        }
    }

    private void ConsumeItem(Consumables consumeItem)
    {
        if (item.gameObject == consumeItem.gameObject)
        {
            Debug.Log("Destroyed");
            Destroy(consumeItem.gameObject);
            item = null;
        }
    }

    public void SetRecieverForConsumables(Consumables itemConsumed)
    {
        consumeListener = itemConsumed;
        consumeListener.OnConsume += ConsumeItem;
        Debug.Log("Added Listener");
    }
}
