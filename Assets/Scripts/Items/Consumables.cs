using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

enum RegenType
{
    Health,
    Mana
}

public class Consumables : Item
{
    [SerializeField] private RegenType regenType;
    [SerializeField] public bool isInstant;
    [SerializeField] private float duration;
    [SerializeField] private float value;

    public GameObject Target;
    public Action<float> Ontick;
    public Action<Consumables> OnConsume;

    private void Start()
    {
        hero = GameObject.FindObjectOfType<PlayerController>().GetComponent<Hero>();
        hero.GetComponent<DamageReceiver>().OnDamaged.AddListener((arg0 =>
        {
            isActivate = false;
        }));
    }

    public override void UseItem()
    {
        Consume();
    }

    public void Consume()
    {
        if (!isInstant)
        {
            switch (regenType)
            {
                case RegenType.Health: StartCoroutine(OnHpRegen());
                    break;
                case RegenType.Mana: StartCoroutine(OnManaRegen());
                    break;
            }
        }
        else
        {
            // Target.GetComponent<Hero>().ManaRegen += value;
            // WhileConsuming();
            hero.GetComponentInChildren<HealthComponent>().HealHP(85);
            hero.Damage += 2;
            OnConsume?.Invoke(this);
            //Inventory.RemoveItem(this);
        }
    }

    private void WhileConsuming()
    {
        if (duration > 0)
        {
            if (!Target.GetComponent<Hero>().isTakingDamage)
            {
                StartCoroutine(ConsumeDuration());

            }
        }
        else
        {
            Target.GetComponent<Hero>().ManaRegen -= value;
            OnConsume?.Invoke(this);
        }
    }

    IEnumerator ConsumeDuration()
    {
        yield return new WaitForSeconds(1);
        duration--;
        Debug.Log("Regenerating" + duration);
        WhileConsuming();
    }
    private void HealHP()
    {
        Target.GetComponent<Unit>().GetHealthComponent().HealHP(value);
        OnConsume?.Invoke(this);
    }

    private void HealMana()
    {
        Target.GetComponent<Hero>().GetManaComponent().AddMana(value);
        OnConsume?.Invoke(this);
    }

    IEnumerator OnManaRegen()
    {
        float timer = duration + Time.time;
        isActivate = true;
        hero.ManaRegen += value;
        while (timer > Time.time && isActivate)
        {
            yield return new WaitForFixedUpdate();
        }
        hero.ManaRegen -= value;
        OnConsume?.Invoke(this);
        //Inventory.RemoveItem(this);
    }
    
    IEnumerator OnHpRegen()
    {
        float timer = duration + Time.time;
        isActivate = true;
        hero.HealthRegen += value;
        while (timer > Time.time && isActivate)
        {
            yield return new WaitForFixedUpdate();
        }
        hero.HealthRegen -= value;
        OnConsume?.Invoke(this);
        //Inventory.RemoveItem(this);
    }
}
