using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Stackable,
    NonStackable
}

public class Item : MonoBehaviour
{
    [SerializeField] public ItemType itemType;

    public Hero hero;
    public InventoryComponent Inventory;
    public int cost;
    public Sprite icon;
    protected bool isActivate = false;

    public virtual void UseItem()
    {
        
    }

    public void BuyItem()
    {
        switch (itemType)
        {
            case ItemType.Stackable:
                break;
            case ItemType.NonStackable:
                break;
        }
    }
}
