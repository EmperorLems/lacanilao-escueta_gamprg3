using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHoverDescription : MonoBehaviour
{
    private GameObject descriptionPanel;

    private void Awake()
    {
        descriptionPanel = this.transform.GetChild(0).gameObject;
    }

    void Update()
    {
        
    }

    private void OnMouseOver()
    {
        Debug.Log(descriptionPanel.name);
        descriptionPanel.SetActive(true);
    }

    private void OnMouseExit()
    {
        descriptionPanel.SetActive(false);
    }
}
