using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : MonoBehaviour
{
    [SerializeField] private CreepSpawner spawner;

    private void OnDestroy()
    {
        spawner.barracks.Remove(this);
    }
}
