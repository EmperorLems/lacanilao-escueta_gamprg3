﻿using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class PlayerController : HeroController
{
    private ParticleSystem clickIndicator;

    [HideInInspector] public UnityEvent<GameObject, Vector3> OnLeftClickEvent;

    private Vector3 hitPos;
    private Camera cam;
    private LayerMask layer;
    protected override void Setup()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.angularSpeed = turnSpeed;
        
        unit = GetComponent<Unit>();
        animator = GetComponent<Animation>();
        
        cam = Camera.main;
        hero = unit.GetComponent<Hero>();
        abilityUser = hero.getAbilityUser();
        layer = GameManager.instance.ClickableLayer;

        clickIndicator = GameObject.FindGameObjectWithTag("AttackIndicator").GetComponent<ParticleSystem>();
        AttackZone = hero.attackZone;
    }

    // Input
    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) LeftClick();

        if (Input.GetMouseButtonDown(1)) RightClick();

        // Stop it, cancel abilities
        if (Input.GetKeyDown(KeyCode.S))
        {
            hero.state = UnitState.Idle;
            abilityUser.CancelActiveAbilities();
        }

        // Ability Controls
        if (Input.GetKeyDown(KeyCode.Q)) hero.getAbilityUser().UseAbility(0);
        
        if (Input.GetKeyDown(KeyCode.W)) hero.getAbilityUser().UseAbility(1);
        
        if (Input.GetKeyDown(KeyCode.E)) hero.getAbilityUser().UseAbility(2);
        
        if (Input.GetKeyDown(KeyCode.R)) hero.getAbilityUser().UseAbility(3);
        
        //if(Input.GetKeyDown(KeyCode.End)) hero.GetHealthComponent().TakeDamage(9999, null);
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            hero.GetInventory().UseItem(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            hero.GetInventory().UseItem(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            hero.GetInventory().UseItem(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            hero.GetInventory().UseItem(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            hero.GetInventory().UseItem(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            hero.GetInventory().UseItem(5);
        }
    }

    private void LeftClick()
    {
        if (!GameManager.instance.getMinimap().isOnMinimapClick(Input.mousePosition))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit, 1000, layer.value))
            {
                OnLeftClickEvent.Invoke(hit.transform.gameObject, hit.point);

                if (GameManager.instance.isInUnitsAndTowerLayer(hit.transform.gameObject))
                {
                    GameManager.instance.GetUnitHUD().SetUnitInUI(hit.transform.gameObject);
                }
                else GameManager.instance.GetUnitHUD().SetUnitInUI(null);
            }
        }
    }

    private void RightClick()
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        //clickIndicator.transform.position = hitPos;
        if (Physics.Raycast(ray, out hit, 1000, layer.value))
        {
            if (GameManager.instance.isInUnitsLayer(hit.transform.gameObject))
            {
                if (hit.transform.gameObject.GetComponent<Unit>().faction != this.GetComponent<Unit>().faction)
                {
                    Stop();
                    SetNewTarget(hit.transform.gameObject);
                }
            }
            else if (GameManager.instance.isTowerLayer(hit.transform.gameObject))
            {
                if (hit.transform.gameObject.GetComponentInParent<Tower>().faction != this.GetComponent<Unit>().faction)
                {
                    Stop();
                    SetNewTarget(hit.transform.gameObject);
                }
            }
            else
            {
                Stop();
                target = null;
                unit.state = UnitState.Follow;
                clickIndicator.transform.position = hit.point;
                clickIndicator.Play();
            }
            hitPos = hit.point;
        }
    }

    #region Override Functions

    protected override void OnFollow()
    {
        PlayWalkAnimation(); 
        MoveToTarget(hitPos);
        if (Vector3.Distance(hitPos, this.transform.position) <= 1) hero.state = UnitState.Idle;
    }

    protected override void OnIdle()
    {
        base.OnIdle();
        hitPos = this.transform.position;
    }
    #endregion
}
