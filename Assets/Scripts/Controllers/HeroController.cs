using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : UnitController
{
    protected Animation animator;
    private TargetsAcquisition targetsAcquisition;
    protected Hero hero;
    protected AbilityUser abilityUser;

    protected override void Setup()
    {
        animator = GetComponent<Animation>();
        targetsAcquisition = GetComponent<TargetsAcquisition>();
        
        targetsAcquisition.Setup(this.transform, unit.faction,true,0.1f);
        
        targetsAcquisition.OnNewTarget.AddListener(OnEnemyNear);
        targetsAcquisition.OnNoEnemyNearby.AddListener(OnNoEnemyNear);

        hero = GetComponent<Hero>();
        abilityUser = hero.getAbilityUser();

        hero.state = UnitState.Follow;
        AttackZone = hero.attackZone;
    }
    

    #region Overrides

    protected override void PlayIdleAnimation()
    {
        animator.enabled = false;
        animator.enabled = true;
        animator.Play("free");
    }
    
    protected override void PlayWalkAnimation()
    {
        animator.Play("walk");
    }

    protected override void PlayAttackAnimation()
    {
        animator.Play("BarbarianAttack");
    }
    
    protected override void PlaySkillAnimation()
    {
        Stop();
        animator.Play("BarbarianSkill");
    }
    
    protected override void PlayDeathAnimation()
    {
        animator.Play("BarbarianDeath");
    }

    protected override void AfterDeathAnimation()
    {
        OnFinishDeathAnimation.Invoke(unit);
    }

    protected override void OnDie()
    {
        PlayDeathAnimation();
    }

    #endregion
}

