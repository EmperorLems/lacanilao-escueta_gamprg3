using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class UnitController : MonoBehaviour
{
    [SerializeField] protected Collider AttackZone;
    public float turnSpeed = 1440;
    public List<Transform> waypoints;
    
    protected Unit unit;
    protected NavMeshAgent agent;

    protected GameObject target;
    protected bool isAttacking = false;


    [HideInInspector] public UnityEvent<Unit> OnFinishDeathAnimation;
    [HideInInspector] public UnityEvent<Unit, DamageReceiver> OnAttackEvent;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.angularSpeed = turnSpeed;
        agent.avoidancePriority = 10;
        
        unit = GetComponent<Unit>();
        SetMovementSpeed(unit.MoveSpeed);
        agent.acceleration = 1000;
        Setup();
    }

    private void FixedUpdate()
    {
        StateUpdate();
    }
    
    protected virtual void Setup(){}

    protected virtual void StateUpdate()
    {
        switch (unit.state)
        {
            case UnitState.Follow: OnFollow();
                break;
            case UnitState.Chase: OnChase();
                break;
            case UnitState.Attack: OnAttack();
                break;
            case UnitState.SpecialAttack: OnSpecialAttack();
                break;
            case UnitState.Channeling: OnChanneling();
                break;
            case UnitState.Die: OnDie();
                break;
            default: OnIdle();
                break;
        }
    }

    #region Movement Functions
    
    private void MoveToWaypoint()
    {
        UpdateWaypoint();
        agent.isStopped = false;
        Vector3 pos = waypoints[0].position;
        pos.y = this.transform.position.y;
        agent.SetDestination(pos);
    }

    protected void MoveToTarget(Vector3 location)
    {
        agent.isStopped = false;
        agent.SetDestination(location);
    }

    protected void Stop()
    {
        agent.isStopped = true;
        agent.ResetPath();
    }

    private void UpdateWaypoint()
    {
        if(waypoints[0] == null) waypoints.RemoveAt(0);
        else
        {
            if (isNearToWaypoint)
            {
                if(waypoints[0].GetComponent<Tower>() == null) waypoints.RemoveAt(0);
            }
        }
    }

    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed);
    }
    #endregion

    #region Bools, Getters and Setters

    public void SetNewTarget(GameObject newTarget)
    {
        target = newTarget;
        unit.state = UnitState.Chase;
    }

    public void SetMovementSpeed(float speedVal)
    {
        if (agent != null)
        {
            agent.speed = speedVal / 10;
        }
    }

    public bool IsStopped() => agent.isStopped;

    public bool IsTargetInFront(Vector3 targetLocation)
    {
        if (Vector3.Distance(this.transform.position, targetLocation) > 10)
        {
            return false;
        }
        Vector3 relativePos = targetLocation - transform.position;
        
        return Vector3.Dot(agent.transform.forward, relativePos) > 0.0f;
    }

    public bool IsTargetInFront()
    {
        if (!GameManager.instance.isInUnitsAndTowerLayer(target)) return false;
        return AttackZone.bounds.Intersects(target.GetComponent<Collider>().bounds);
    }

    public bool isNearToWaypoint => Vector3.Distance(this.transform.position, waypoints[0].position) <= 10;

    public bool isNear(Vector3 location, float acceptableDistance) =>
        Vector3.Distance(this.transform.position, location) >= acceptableDistance;

    public void ResetState() => unit.state = UnitState.Idle;

    #endregion

    #region State Updates

    protected virtual void OnIdle()
    {
        PlayIdleAnimation();
        Stop();
    }
    
    protected virtual void OnChase()
    {
        if (!IsTargetInFront())
        {
            PlayWalkAnimation();
            MoveToTarget(target.transform.position);
        }
        else
        {
            Stop();
            unit.state = UnitState.Attack;
        }
    }

    protected virtual void OnAttack()
    {
        RotateTowards(target.transform);
        if (!isAttacking)
        {

            if (target == null || (target.GetComponent<Unit>() != null &&
                                   target.GetComponent<Unit>().state == UnitState.Die))
            {
                unit.state = UnitState.Follow;
                return;
            }

            if (!IsTargetInFront())
            {
                unit.state = UnitState.Chase;
                return;
            }

            
            StartCoroutine(AttackRoutine());
        }
    }

    protected virtual IEnumerator AttackRoutine()
    {
        if (!isAttacking)
        {
            isAttacking = true;
            PlayAttackAnimation();
            yield return new WaitForSeconds(unit.getAttackInterval());
            isAttacking = false;
        }
    }

    protected virtual void OnSpecialAttack()
    {
        PlaySkillAnimation();
    }

    protected virtual void OnChanneling()
    {
        PlayWalkAnimation();
        Stop();
    }
    protected virtual void OnFollow()
    {
        PlayWalkAnimation(); 
        MoveToWaypoint();
    }

    protected virtual void OnDie()
    {
        Stop();
        PlayDeathAnimation();
    }
    #endregion

    #region Play Animation

    protected virtual void PlayIdleAnimation() { }
    
    protected virtual void PlayWalkAnimation() { }
    protected virtual void PlayAttackAnimation() { }

    protected virtual void PlayDeathAnimation() { }

    protected virtual void PlaySkillAnimation() { }

    #endregion
    
    #region Animation Functions

    protected virtual void OnRangeAttack(HomingProjectile projectilePrefab)
    {
        if (target != null)
        {
            HomingProjectile projectile = Instantiate(projectilePrefab, unit.projectileSpawnPoint.position, Quaternion.identity);
            projectile.Setup(target, unit.Damage, this.gameObject, unit.attackType);
        }
        else Debug.LogError("Target NULL");
    }

    protected virtual void OnMeleeAttack()
    {
        if (AttackZone == null)
        {
            Debug.LogError("Box Collider Null on " + gameObject); 
            return;
        }
        
        if (IsTargetInFront())
        {
            if (target.TryGetComponent(out DamageReceiver receiver))
            {
                bool isSuccessful = receiver.TakeDamage(unit.Damage,unit.attackType, this.gameObject);
                if(isSuccessful) OnAttackEvent.Invoke(unit, receiver);
            }
        }
    }

    protected virtual void AfterDeathAnimation() { }
    #endregion

    #region For Bots Function Only

    protected void OnEnemyNear(Unit enemy)
    {
        if(unit.state == UnitState.Die) return;
        
        target = enemy.gameObject;
        unit.state = UnitState.Chase;
    }

    protected void OnNoEnemyNear(TargetsAcquisition targetsAcquisition)
    {
        //Debug.LogWarning("NoEnemyNear");
        if(unit.state == UnitState.Die) return;

        if (targetsAcquisition.targetUnit != null)
        {
            OnEnemyNear(targetsAcquisition.targetUnit);
            return;
        }
        
        if (targetsAcquisition.IsNearbyTower())
        {
            target = targetsAcquisition.targetTower.gameObject;
            unit.state = UnitState.Chase;
        }
        else
        {
            unit.state = UnitState.Follow;
        }
    }
    

    #endregion
}
