using System.Collections;
using UnityEngine;

public class CreepController : UnitController
{
    private Animator animator;
    private TargetsAcquisition targetsAcquisition;

    protected override void Setup()
    {
        animator = GetComponent<Animator>();
        targetsAcquisition = GetComponent<TargetsAcquisition>();
        
        targetsAcquisition.Setup(this.transform, unit.faction,true,0.1f);
        
        targetsAcquisition.OnNewTarget.AddListener(OnEnemyNear);
        targetsAcquisition.OnNoEnemyNearby.AddListener(OnNoEnemyNear);
    }

    #region Overrides

    protected override void PlayIdleAnimation()
    {
        animator.SetBool("isWalking", false);
        animator.SetBool("isAttacking", false);
    }
    
    protected override void PlayWalkAnimation()
    {
        animator.SetBool("isWalking", true);
    }

    protected override void PlayDeathAnimation()
    {
        animator.SetBool("isWalking", false);
        animator.SetBool("isAttacking", false);
        animator.SetBool("isDead", true);
    }

    protected override void AfterDeathAnimation()
    {
        Destroy(this.gameObject);
    }

    protected override IEnumerator AttackRoutine()
    {
        if (!isAttacking)
        {
            Stop();
            isAttacking = true;
            animator.SetBool("isAttacking", true);
            
            // while(animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 /*&& !animator.IsInTransition(0)*/)
            // {
            //     yield return new WaitForFixedUpdate();
            //     Debug.LogWarning("Waiting " + animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
            // }
            
            yield return new WaitForSeconds(unit.getAttackInterval());
            
            animator.SetBool("isAttacking", false);
            isAttacking = false;
        }
    }

    #endregion
}
