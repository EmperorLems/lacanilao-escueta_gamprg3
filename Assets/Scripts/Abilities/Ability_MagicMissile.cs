using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_MagicMissile : AbilityBase
{
    // always leave first item in array for level 0
    [Header("Upgradable Variables")]
    [SerializeField] private float[] damagePerUpgrade = new float[5];
    [SerializeField] private float[] stunDurationPerUpgrade = new float[5];

    [Header("Prefabs")]
    [SerializeField] private HomingProjectile projectile_prefab;
    [SerializeField] private StunStatusEffect _Stun;
    private float currentDamage => damagePerUpgrade[Level];
    private float currentStunDuration => stunDurationPerUpgrade[Level];
    
    private GameObject target;

    protected override void OnActivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.AddListener(OnLeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.RemoveListener(OnLeftClick);
            target = null;
        }
    }

    void OnLeftClick(GameObject obj, Vector3 location)
    {
        if (!CheckIfStillActive()) return;
        if (obj.TryGetComponent(out Unit unit))
        {
            if (unit.faction == hero.faction) return;
            
            if (IsWithinCastRange(unit.transform.position))
            {
                target = obj;
                hero.transform.LookAt(target.transform.position);
                OnFinishTargeting.Invoke(this);
            }
        }
    }

    protected override void OnAbilityAttack()
    {
        HomingProjectile projectile = Instantiate(projectile_prefab, hero.getSpawnPoint().position, 
            hero.gameObject.transform.rotation);
        
        projectile.Setup(target, currentDamage, hero.gameObject, AttackType.Hero, IsMagicalDmg);
        projectile.OnHit.AddListener(OnProjectileHit);
    }

    private void OnProjectileHit(GameObject target)
    {
        StatusEffectReceiver receiver = target.GetComponent<StatusEffectReceiver>();

        StunStatusEffect s = Instantiate(_Stun, receiver.transform);
        s.Duration = currentStunDuration;
        s.ID = receiver.GetInstanceID() + this.GetInstanceID();
        receiver.ApplyStatusEffect(s, hero.gameObject);
    }
}
