using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_GuardianSpirit : AbilityBase
{
    [SerializeField] private float[] moveSpeedBonus = new float[5];
    [SerializeField] private float[] atkSpeedBonus = new float[5];

    [SerializeField] float effectDuration;

    [SerializeField] private SlowStatusEffect _slow;
    [SerializeField] private AttackSpeedBonusEffect _atkSpd;

    private List<StatusEffect> effectApplied = new List<StatusEffect>();
    protected override void OnActivate()
    {
        if (hero.TryGetComponent(out StatusEffectReceiver receiver))
        {
            SlowStatusEffect slowStatusEffect = Instantiate(_slow, receiver.transform);
            slowStatusEffect.speedMod = (1 + moveSpeedBonus[Level]);
            slowStatusEffect.ID = GetInstanceID() + receiver.GetInstanceID();
            slowStatusEffect.Duration = effectDuration;
            effectApplied.Add(slowStatusEffect);

            AttackSpeedBonusEffect atkSpdEffect = Instantiate(_atkSpd, receiver.transform);
            atkSpdEffect.speedMod = atkSpeedBonus[Level];
            atkSpdEffect.ID = GetInstanceID() + receiver.GetInstanceID();
            atkSpdEffect.Duration = effectDuration;
            effectApplied.Add(atkSpdEffect);
            
            receiver.ApplyStatusEffect(slowStatusEffect);
            receiver.ApplyStatusEffect(atkSpdEffect);

            StartCoroutine(RoutineWithDuration(effectDuration, 1));
        }
    }

    protected override void OnDeactivate()
    {
        foreach (StatusEffect effect in effectApplied)
        {
            effect.RemoveFromReceiver();
        }
    }
}
