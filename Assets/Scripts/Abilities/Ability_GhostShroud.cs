using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ability_GhostShroud : AbilityBase
{
    [Header("Non-Upgradable Variables")]
    [SerializeField] private float[] speedModPerUpgrade = new float[5];
    [SerializeField] private float[] durationPerUpgrade = new float[5];

    [Header("Prefabs")]
    [SerializeField] private SlowStatusEffect _slow;
    [SerializeField] private DisarmEffect _disarm;
    [SerializeField] private PhysicalImmuneEffect _immune;

    private TargetsAcquisition _targetsAcquisition;
    public float currentSpeedMod => speedModPerUpgrade[Level];
    public float currentDuration => durationPerUpgrade[Level];

    private List<StatusEffect> effectsApplied = new List<StatusEffect>();

    protected override void OnSetup()
    {
        _targetsAcquisition = GetComponent<TargetsAcquisition>();
        _targetsAcquisition.Setup(hero.transform, hero.faction, false);
    }

    protected override void OnActivate()
    {
        foreach (AbilityBase ability in abilityUser.abilities)
        {
            if (ability.TryGetComponent(out Ability_DeathPulse deathPulse))
            {
                deathPulse.healMod += 1;
                break;
            }
        }
        
        hero.StackMagicResist(-0.4f);

        GiveHeroEffects();
        
        StartCoroutine(RoutineWithDuration(currentDuration, 0.1f));
    }

    protected override void OnDeactivate()
    {
        foreach (StatusEffect effect in effectsApplied)
        {
            effect.RemoveFromReceiver();
        }
        
        foreach (AbilityBase ability in abilityUser.abilities)
        {
            if (ability.TryGetComponent(out Ability_DeathPulse deathPulse))
            {
                deathPulse.healMod -= 1;
                break;
            }
        }
        
        hero.RemoveMagicResistStack(-0.4f);
        RemoveEffects();
    }

    protected override void OnTick()
    {
        foreach (Unit enemyUnit in _targetsAcquisition.GetNearbyEnemyUnits(CastRange/5))
        {
            if (enemyUnit.TryGetComponent(out StatusEffectReceiver receiver))
            {
                GiveSlowEffect(receiver);
            }
            
            // remove all null effect
            effectsApplied = effectsApplied.Where(aa => aa != null).ToList();
            
            // remove all effects that are far from hero
            List<StatusEffect> effectToRemove = effectsApplied.Where(aa =>
                Vector3.Distance(transform.position, aa.transform.position) >= 20).ToList();
            
            foreach (StatusEffect slow in effectToRemove)
            {
                Debug.Log(slow.gameObject);
                effectsApplied.Remove(slow);
                slow.RemoveFromReceiver();
                int ID = receiver.GetInstanceID() + this.GetInstanceID();
            }
        }
    }

    void GiveHeroEffects()
    {
        StatusEffectReceiver receiver = hero.GetComponent<StatusEffectReceiver>();

        DisarmEffect disarm = Instantiate(_disarm, hero.transform);
        disarm.Duration = currentDuration;
        //disarm.ID = this.GetInstanceID() + receiver.GetInstanceID();
        
        receiver.ApplyStatusEffect(disarm);
        effectsApplied.Add(disarm);
        
        if (!receiver.isDuplicateByType<PhysicalImmuneEffect>())
        {
            PhysicalImmuneEffect immune = Instantiate(_immune, hero.transform);
            immune.Duration = currentDuration;
            
            receiver.ApplyStatusEffect(immune);
            effectsApplied.Add(immune);
        }
    }

    void RemoveEffects()
    {
        if (hero.TryGetComponent(out StatusEffectReceiver receiver))
        {
            receiver.RemoveStatusEffect(this.GetInstanceID() + receiver.GetInstanceID());
        }
    }

    void GiveSlowEffect(StatusEffectReceiver receiver)
    {
        int ID = receiver.GetInstanceID() + this.GetInstanceID();
        if (!receiver.isDuplicate(ID))
        {
            SlowStatusEffect s = Instantiate(_slow, receiver.transform);
            s.Duration = currentDuration;
            s.ID = ID;
            s.speedMod = 1 - currentSpeedMod;
            receiver.ApplyStatusEffect(s, this.gameObject);
        }
    }
}
