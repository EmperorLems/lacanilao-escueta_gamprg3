using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_ReapersScythe : AbilityBase
{
    [SerializeField]private float[] dmgPerMissingHp = new float[4];
    [SerializeField] private float stunDur;

    [SerializeField] private StunStatusEffect _stun;
    [SerializeField] private HomingProjectile _projectile;

    private Unit enemyUnit;
    protected override void OnActivate()
    {
        if (hero.TryGetComponent(out PlayerController controller))
        {
            controller.OnLeftClickEvent.AddListener(OnLeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.TryGetComponent(out PlayerController controller))
        {
            controller.OnLeftClickEvent.RemoveListener(OnLeftClick);
        }
    }

    private void OnLeftClick(GameObject obj, Vector3 loc)
    {
        if(!CheckIfStillActive()) return;
        if (obj.TryGetComponent(out Unit unit))
        {
            if(unit.faction == hero.faction) return;
            if(!IsWithinCastRange(loc)) return;
            
            hero.transform.LookAt(loc);
            enemyUnit = unit;
            OnFinishTargeting.Invoke(this);
        }
    }

    protected override void OnAbilityAttack()
    {
        HomingProjectile proj = Instantiate(_projectile, hero.getSpawnPoint().position, hero.getSpawnPoint().rotation);
        proj.Setup(enemyUnit.gameObject);
        proj.OnHit.AddListener(OnHit);
    }

    private void OnHit(GameObject enemy)
    {
        if (enemy != enemyUnit.gameObject) return;
        
        if (enemyUnit.TryGetComponent(out StatusEffectReceiver receiver))
        {
            StunStatusEffect stunEffect = Instantiate(_stun, receiver.transform);
            stunEffect.Duration = stunDur;
            stunEffect.ID = GetInstanceID() + receiver.GetInstanceID();
            
            stunEffect.OnEffectEnd.AddListener(OnStunEnd);
            receiver.ApplyStatusEffect(stunEffect);
        }
    }

    private void OnStunEnd(StatusEffect effect, StatusEffectReceiver receiver)
    {
        if (receiver.TryGetComponent(out Unit unit))
        {
            float missingHP = unit.GetHealthComponent().GetMaxHealth() - unit.GetHealthComponent().GetCurrentHealth();
            float dmg = missingHP * dmgPerMissingHp[Level];
            
            unit.GetComponent<DamageReceiver>().TakeDamage(dmg,AttackType.Hero,hero.gameObject,true);
        }
    }
}
