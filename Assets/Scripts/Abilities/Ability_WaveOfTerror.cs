using UnityEngine;

public class Ability_WaveOfTerror : AbilityBase
{
    [Header("Prefabs")]
    [SerializeField] private NonHomingProjectile projectilePrefab; 
    [SerializeField] private ReduceArmor _reduceArmor;
    
    [Header("Non-Upgradable Variables")]
    [SerializeField] private float effectDuration;
    
    // always leave first item in array for level 0
    [Header("Upgradable Variables")]
    [SerializeField] private float[] damagePerUpgrade = new float[5];
    [SerializeField] private float[] armorReductionPerUpgrade = new float[5];

    private float currentArmorReduction => armorReductionPerUpgrade[Level];
    private float currentDamage => damagePerUpgrade[Level];
    

    protected override void OnActivate()
    {
        // if (hero.GetComponent<PlayerController>() != null)
        // {
        //     PlayerController pc = hero.GetComponent<PlayerController>();
        //     pc.OnLeftClickEvent.AddListener(OnLeftClick);
        // }

        if (hero.TryGetComponent(out PlayerController controller))
        {
            controller.OnLeftClickEvent.AddListener(OnLeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.RemoveListener(OnLeftClick);
        }
    }

    private void OnLeftClick(GameObject obj, Vector3 targetPos)
    {
        if(!CheckIfStillActive()) return;

        if (IsWithinCastRange(targetPos))
        {
            hero.transform.LookAt(targetPos);
            OnFinishTargeting.Invoke(this);
        }
    }

    protected override void OnAbilityAttack()
    {
        NonHomingProjectile projectile =
            Instantiate(projectilePrefab, hero.getSpawnPoint().position, hero.transform.rotation);
        projectile.Setup(currentDamage, 140.0f, hero.gameObject, hero.faction, AttackType.Hero, 200.0f, false, true);
        projectile.OnHit.AddListener(OnProjectileHit);
    }

    private void OnProjectileHit(GameObject target)
    {
        if(target.layer == 6 || target.layer == 7|| target.layer == 10)
        {
            if (target.GetComponent<Unit>().faction != hero.faction)
            {
                StatusEffectReceiver receiver = target.GetComponent<StatusEffectReceiver>();

                ReduceArmor r = Instantiate(_reduceArmor, receiver.transform);
                r.reduceVal = currentArmorReduction;
                r.Duration = effectDuration;
                r.ID = receiver.GetInstanceID() + this.GetInstanceID();
                receiver.ApplyStatusEffect(r, hero.gameObject);
            }
        }
    }
}
