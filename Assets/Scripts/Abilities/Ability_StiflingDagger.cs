using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_StiflingDagger : AbilityBase
{
    // always leave first item in array for level 0
    [Header("Upgradable Variables")]
    [SerializeField] private float[] dmgModPerUpgrade = new float[5];
    [SerializeField] private float[] effectDurationPerUpgrade = new float[5];

    [Header("Prefabs")]
    [SerializeField] private HomingProjectile projectile_prefab;
    [SerializeField] private SlowStatusEffect _Slow;
    private float currentDmgMod => dmgModPerUpgrade[Level];
    private float currentEffectDuration => effectDurationPerUpgrade[Level];
    
    private GameObject target;

    protected override void OnActivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.AddListener(OnLeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.RemoveListener(OnLeftClick);
            target = null;
        }
    }

    void OnLeftClick(GameObject obj, Vector3 location)
    {
        if (!CheckIfStillActive()) return;
        if (obj.TryGetComponent(out Unit unit))
        {
            if(unit.faction == hero.faction) return;
            if (!IsWithinCastRange(obj.transform.position)) return;
            
            target = obj;
            hero.transform.LookAt(target.transform.position);
            OnFinishTargeting.Invoke(this);
        }
    }

    protected override void OnAbilityAttack()
    {
        HomingProjectile projectile = Instantiate(projectile_prefab, hero.getSpawnPoint().position, 
            hero.gameObject.transform.rotation);

        float damage = abilityUser.GetComponent<Hero>().Damage * currentDmgMod;
        projectile.Setup(target, damage, hero.gameObject, AttackType.Hero, IsMagicalDmg, 20f, 1200);
        projectile.OnHit.AddListener(OnProjectileHit);
    }

    private void OnProjectileHit(GameObject target)
    {
        StatusEffectReceiver receiver = target.GetComponent<StatusEffectReceiver>();
        
        int ID = receiver.GetInstanceID() + _Slow.GetInstanceID();
        if(receiver.isDuplicate(ID)) return;
        
        SlowStatusEffect s = Instantiate(_Slow, receiver.transform);
        s.Duration = currentEffectDuration;
        s.ID = ID;

        receiver.ApplyStatusEffect(s, this.gameObject);
    }
}
