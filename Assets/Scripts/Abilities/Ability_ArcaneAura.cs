using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_ArcaneAura : AbilityBase
{
    [SerializeField] private float[] manaRegenBonus = new float[5];
    [SerializeField] private RegenBonusEffect effectPrefab;

    private List<RegenBonusEffect> _effectsApplied = new List<RegenBonusEffect>();

    protected override void OnUpgrade()
    {
        foreach (RegenBonusEffect regen in _effectsApplied)
        {
            int mod = (regen.Target.gameObject == hero.gameObject) ? 3 : 1;
            regen.UpdateBonus(0,manaRegenBonus[Level] * mod);
        }
    }

    protected override void OnActivate()
    {
        List<Hero> heroes = (hero.faction == Faction.Dire)
            ? GameManager.instance.getUnitManager().DireHeroes
            : GameManager.instance.getUnitManager().RadiantHeroes;

        foreach (Hero h in heroes)
        {
            if(h.TryGetComponent(out StatusEffectReceiver receiver))
            {
                GiveEffect(receiver);
            }
        }
    }

    void GiveEffect(StatusEffectReceiver receiver)
    {
        int mod = (receiver.gameObject == hero.gameObject) ? 3 : 1;
        RegenBonusEffect regen = Instantiate(effectPrefab, receiver.transform);
        regen.ManaRegenBonus = manaRegenBonus[Level] * mod;
        regen.HpRegenBonus = 0;
        regen.removeOnDeath = false;
        regen.ID = receiver.GetInstanceID() + GetInstanceID();
        regen.Duration = 0;
        receiver.ApplyStatusEffect(regen);
        _effectsApplied.Add(regen);
    }
}
