using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum AbilityState
{
    Standby,
    Active,
    Cooldown,
}

public class AbilityBase : MonoBehaviour
{
    [Header("Non-Upgradable Base Variables")]
    [SerializeField] private bool isUltimate;
    [SerializeField] private bool isPassive;
    [SerializeField] private bool isChanneled;
    [SerializeField] private bool isMagicalDmg;
    [SerializeField] private bool isDispellable;
    [SerializeField] private Sprite icon;
    
    [Header("Upgradable Base Variables")]
    [SerializeField] protected float[] castRangePeUpgrade = new float[5];
    [SerializeField] protected float[] manaCostPerUpgrade = new float[5];
    [SerializeField] protected float[] cooldownDurationPerUpgrade = new float[5];

    [HideInInspector] public UnityEvent<AbilityState> OnChangeState;
    [HideInInspector] public UnityEvent<AbilityBase> OnFinishTargeting;
    [HideInInspector] public UnityEvent<AbilityBase> OnChanneling;

    protected Hero hero;
    protected AbilityUser abilityUser;
    protected AbilityState abilityState;
    protected float cooldownTimer;
    private int abilityLevel = 0;
    private float channelingDur = 0;
    private float channelingTimer = 0;
    
    public void Setup(Hero owner, AbilityUser user)
    {
        hero = owner;
        abilityUser = user;
        cooldownTimer = 0;
        abilityState = AbilityState.Standby;
        OnChangeState.Invoke(abilityState);
        OnSetup();
        UpgradeAbility();
    }

    private void OnEnable()
    {
        if (isPassive && TryGetComponent(out TargetsAcquisition targetsAcquisition))
        {
            //Debug.Log("ShouldStart");
            if (targetsAcquisition.AlreadySetup)
            {
                StartCoroutine(PassiveRoutine(.1f));
            }
        }
    }

    #region Getters & Setter
    
    public bool IsUltimate
    {
        get => isUltimate;
    }
    
    public bool IsPassive
    {
        get => isPassive;
    }

    public bool IsChanneled
    {
        get => isChanneled;
        set => isChanneled = value;
    }

    public bool IsMagicalDmg
    {
        get => isMagicalDmg;
        set => isMagicalDmg = value;
    }

    public float ChannelledTimer => channelingTimer;
    public float channelledDur => channelingDur;

    public float CastRange => castRangePeUpgrade[Level];

    public float ManaCost => manaCostPerUpgrade[Level];

    public float CooldownDuration => cooldownDurationPerUpgrade[Level];

    public AbilityState GetAbilityState
    {
        get => abilityState;
    }
    

    public float CooldownTimer
    {
        get => cooldownTimer;
        set => cooldownTimer = value;
    }

    public Sprite Icon { get => icon; }

    public int Level => abilityLevel;

    #endregion

    
    protected virtual void OnActivate() { }

    // Deactivating without cooldown
    protected virtual void OnDeactivate() { }
    
    protected virtual void OnAbilityAttack() { }
    protected virtual void OnUpgrade() {}

    protected virtual void OnSetup() { }
    public virtual bool IsMaxUpgraded()
    {
        if (isUltimate)
        {
            return Level >= 3;
        }
        else return Level >= 4;
    }
    // for using the ability
    public void Activate()
    {
        if (abilityState == AbilityState.Standby)
        {
            abilityState = AbilityState.Active;
            OnChangeState.Invoke(abilityState);
            OnActivate();
        }
    }
    
    // for starting cooldown
    public void Deactivate()
    {
        hero.GetManaComponent().SubtractMana(ManaCost);
        OnDeactivate();
        
        StartCoroutine(StartCooldown());
    }

    public void UpgradeAbility()
    {
        if (!IsMaxUpgraded())
        {
            abilityLevel++;
            OnUpgrade();
        }
    }
    
    // called when skill animation is finished
    public void AbilityAttack()
    {
        if (CheckIfStillActive())
        {
            OnAbilityAttack();
            Deactivate();
        }
    }
    
    private IEnumerator StartCooldown()
    {
        abilityState = AbilityState.Cooldown;
        OnChangeState.Invoke(abilityState);
        cooldownTimer = 0;

        while (cooldownTimer <= CooldownDuration)
        {
            yield return new WaitForSeconds(0.1f);
            cooldownTimer+= 0.1f;
        }
        
        abilityState = AbilityState.Standby;
        OnChangeState.Invoke(abilityState);
    }

    protected bool IsWithinCastRange(Vector3 location)
    {
        //Debug.LogWarning("Distance: " + Vector3.Distance(hero.transform.position, location) + "  Cast Range: " + castRange);
        return Vector3.Distance(hero.transform.position, location) <= CastRange/10;
    }

    protected bool CheckIfStillActive()
    {
        if (abilityState != AbilityState.Active)
        {
            OnDeactivate();
            return false;
        }
        return true;
    }
    
    // call this for canceling outside of this class
    public void Cancel()
    {
        if (isPassive) return;
        if(abilityState == AbilityState.Cooldown) return;
        
        abilityState = AbilityState.Standby;
        OnChangeState.Invoke(abilityState);
        OnDeactivate();
    }

    protected IEnumerator RoutineWithDuration(float duration, float tick)
    {
        float timer = 0;
        
        while (timer <= duration)
        {
            OnTick();
            yield return new WaitForSeconds(tick);
            timer+= tick;
        }
        Deactivate();
    }

    protected IEnumerator PassiveRoutine(float tick)
    {
        while (abilityState == AbilityState.Active)
        {
            OnTick();
            yield return new WaitForSeconds(tick);
        }
    }

    protected IEnumerator ChannellingRoutine(float duration)
    {
        // float StartTime = Time.time;
        channelingDur = duration;
        channelingTimer = 0;
        // float timer = Time.time + duration;
        hero.state = UnitState.Channeling;
        OnChanneling?.Invoke(this);
        while (channelingTimer < channelingDur && hero.state == UnitState.Channeling)
        {
            hero.state = UnitState.Channeling;
            OnChannelling();
            yield return new WaitForSeconds(0.1f);
            channelingTimer += .1f;
        }

        if (hero.state == UnitState.Channeling)
        {
            OnFinishTargeting.Invoke(this);
        }
        else
        {
            Cancel();
        }
    }
    
    protected virtual void OnTick() { }
    
    protected virtual void OnChannelling() {}

    protected virtual void OnDispel() {}
    public void Dispel()
    {
        if(!isDispellable) return;
        OnDispel();
        Deactivate();
    }
}
