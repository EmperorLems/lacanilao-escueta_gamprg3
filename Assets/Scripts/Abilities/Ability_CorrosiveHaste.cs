using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_CorrosiveHaste : AbilityBase
{
    [SerializeField] private float[] ArmorReduction;

    [SerializeField] private float effectDur;
    [SerializeField] private ReduceArmor _reduceArmor;
    private LayerMask layers;

    [SerializeField]private HomingProjectile _projectile;
    private GameObject target;
    protected override void OnSetup()
    {
        layers = GameManager.instance.UnitsLayers;
    }

    protected override void OnActivate()
    {
        if (hero.TryGetComponent(out PlayerController controller))
        {
            controller.OnLeftClickEvent.AddListener(OnLeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.TryGetComponent(out PlayerController controller))
        {
            controller.OnLeftClickEvent.RemoveListener(OnLeftClick);
        }
    }

    private void OnLeftClick(GameObject obj, Vector3 loc)
    {
        if(!CheckIfStillActive()) return;
        if (obj.TryGetComponent(out Unit unit))
        {
            if(unit.faction == hero.faction) return;
            if(!IsWithinCastRange(loc)) return;
            
            target = obj;
            OnFinishTargeting.Invoke(this);
        }
    }

    protected override void OnAbilityAttack()
    {
        HomingProjectile proj = Instantiate(_projectile, hero.getSpawnPoint().position, hero.transform.rotation);
        proj.Setup(target);
        proj.OnHit.AddListener((hitObj =>
        {
            if(hitObj!=target) return;

            if (target.TryGetComponent(out StatusEffectReceiver effectReceiver))
            {
                GiveArmorReduction(effectReceiver);
            }
            
        }));
    }

    private void GiveArmorReduction(StatusEffectReceiver receiver)
    {
        int effectID = receiver.GetInstanceID() + GetInstanceID();

        if (!receiver.isDuplicate(effectID))
        {
            ReduceArmor reduce = Instantiate(_reduceArmor, receiver.transform);
            reduce.reduceVal = ArmorReduction[Level];
            reduce.Duration = effectDur;
            
            receiver.ApplyStatusEffect(reduce);
        }
    }
}
