using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class Ability_VengeanceAura : AbilityBase
{
    [Header("Prefabs")]
    [SerializeField] private VengeanceStatusEffect _vengeance;

    [Header("Upgradable Variables")]
    [SerializeField] private float[] dmgBonusPerLevel = new float[5];
    [SerializeField] private float[] rngBonusPerLevel = new float[5];
    
    private TargetsAcquisition targetsAcquisition;
    
    private float currentDmgMultBonus => dmgBonusPerLevel[Level];
    private float currentRngMultBonus => rngBonusPerLevel[Level];
    public float tick = 0.5f;
    
    private List<VengeanceStatusEffect> vengeanceApplied = new List<VengeanceStatusEffect>();

    protected override void OnSetup()
    {
        targetsAcquisition = GetComponent<TargetsAcquisition>();
        targetsAcquisition.Setup(hero.transform, hero.faction, false); //,0.2f,CastRange/10,false);
    }

    protected override void OnActivate()
    {
        abilityState = AbilityState.Active;
        StartCoroutine(PassiveRoutine(tick));
    }

    protected override void OnDeactivate()
    {
        StopCoroutine(PassiveRoutine(tick));

        foreach (VengeanceStatusEffect ven in vengeanceApplied)
        {
            RemoveVengeance(ven);
        }
    }

    private void GiveVengeance(StatusEffectReceiver receiver)
    {
        int ID = this.GetInstanceID() + receiver.GetInstanceID() + _vengeance.GetInstanceID();
        
        if (!receiver.isDuplicate(ID))
        {
            VengeanceStatusEffect effect = Instantiate(_vengeance, receiver.transform);
            
            effect.damageMultBonus = currentDmgMultBonus;
            effect.rangeMultBonus = currentRngMultBonus;
            effect.ID = ID;
            
            receiver.ApplyStatusEffect(effect, this.gameObject);
        
            vengeanceApplied.Add(effect);
        }
    }

    private void RemoveVengeance(VengeanceStatusEffect ven)
    {
        ven.RemoveFromReceiver();
        vengeanceApplied.Remove(ven);
    }

    protected override void OnTick()
    {
        foreach (Unit unit in targetsAcquisition.GetNearbyFriendlyUnits(CastRange/10))
        {
            if (unit.attackType == AttackType.Pierce)
            {
                if (unit.TryGetComponent(out StatusEffectReceiver receiver))
                {
                    GiveVengeance(receiver);
                }
            }
        }

        // remove all null bonus armor effect
        vengeanceApplied = vengeanceApplied.Where(aa => aa != null).ToList();
            
        // remove all effects that are far from hero
        List<VengeanceStatusEffect> vengeanceToRemove = vengeanceApplied.Where(aa =>
            Vector3.Distance(transform.position, aa.transform.position) >= 30).ToList();

        foreach (VengeanceStatusEffect ven in vengeanceToRemove)
        {
            RemoveVengeance(ven);
        }
    }
}
