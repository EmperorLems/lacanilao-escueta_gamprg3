using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_CrystalNova : AbilityBase
{
    [SerializeField] private float[] Damage = new float[5];
    [SerializeField] private float[] MoveSpeedSlow = new float[5];
    [SerializeField] private float[] AttackSpeedSlow = new float[5];

    [SerializeField] private float effectDur = 4.5f;
    [SerializeField] private NonHomingProjectile _projectile;
    [SerializeField] private SlowStatusEffect _slow;
    [SerializeField] private AttackSpeedBonusEffect _atkSped;
    protected override void OnActivate()
    {
        if (hero.TryGetComponent(out PlayerController controller))
        {
            controller.OnLeftClickEvent.AddListener(LeftClick);
        }
    }

    void LeftClick(GameObject obj, Vector3 loc)
    {
        if(!CheckIfStillActive()) return;

        if (IsWithinCastRange(loc))
        {
            hero.transform.LookAt(loc);
            OnFinishTargeting.Invoke(this);
        }
    }
    
    protected override void OnAbilityAttack()
    {
        NonHomingProjectile projectile =
            Instantiate(_projectile, hero.getSpawnPoint().position, hero.transform.rotation);
        projectile.Setup(Damage[Level], 140.0f, hero.gameObject, hero.faction, AttackType.Hero, 120.0f, false, true);
        projectile.OnHit.AddListener(OnProjectileHit);
    }

    private void OnProjectileHit(GameObject target)
    {
        if (target.TryGetComponent(out Unit unit))
        {
            if(unit.faction == hero.faction) return;

            if (unit.TryGetComponent(out StatusEffectReceiver receiver))
            {
                int slowID = _slow.GetInstanceID() + receiver.GetInstanceID();
                
                if (!receiver.isDuplicate(slowID))
                {
                    SlowStatusEffect slowStatusEffect = Instantiate(_slow, receiver.transform);
                    slowStatusEffect.speedMod = 1 - MoveSpeedSlow[Level];
                    slowStatusEffect.Duration = effectDur;
                    slowStatusEffect.ID = slowID;
                    receiver.ApplyStatusEffect(slowStatusEffect);
                }

                int atkID = _atkSped.GetInstanceID() + receiver.GetInstanceID();

                if(!receiver.isDuplicate(atkID))
                {
                    AttackSpeedBonusEffect atkSpeedBonusEffect = Instantiate(_atkSped, receiver.transform);
                    atkSpeedBonusEffect.speedMod = -AttackSpeedSlow[Level];
                    atkSpeedBonusEffect.Duration = effectDur;
                    atkSpeedBonusEffect.ID = atkID;
                    receiver.ApplyStatusEffect(atkSpeedBonusEffect);
                }
            }
        }
    }
}
