using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_DeathPulse : AbilityBase
{
    [Header("Upgradable Variables")]
    [SerializeField] private float[] dmgPerUpgrade = new float[5];
    [SerializeField] private float[] healPerUpgrade = new float[5];

    private TargetsAcquisition _targetsAcquisition;

    private float dmg => dmgPerUpgrade[Level];
    private float heal => healPerUpgrade[Level];

    public float healMod = 1;

    protected override void OnSetup()
    {
        _targetsAcquisition = GetComponent<TargetsAcquisition>();
        _targetsAcquisition.Setup(hero.transform, hero.faction, false);
    }

    protected override void OnActivate()
    {
        OnFinishTargeting.Invoke(this);
    }

    protected override void OnAbilityAttack()
    {
        foreach (Unit enemy in _targetsAcquisition.GetNearbyEnemyUnits(CastRange/5))
        {
            
            if (enemy.TryGetComponent(out DamageReceiver receiver))
            {
                receiver.TakeDamage(dmg, AttackType.Hero, hero.gameObject, IsMagicalDmg);
            }
        }

        foreach (Unit ally in _targetsAcquisition.GetNearbyFriendlyUnits(CastRange/10, hero.faction))
        {
            ally.GetHealthComponent().HealHP(heal*healMod);
        }
    }
}
