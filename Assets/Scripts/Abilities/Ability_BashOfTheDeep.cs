using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_BashOfTheDeep : AbilityBase
{
    [SerializeField] private float[] Damage = new float[5];
    [SerializeField] private float[] stunDur = new float[5];
    [SerializeField] private StunStatusEffect _stun;
    [SerializeField] private BonusDamageEffect _bonusDmgeffect;
    private int requiredHits = 4;
    private int hitsCounter = 0;
    private BonusDamageEffect appliedEffect;
    protected override void OnSetup()
    {
        if (hero.TryGetComponent(out HeroController controller))
        {
            controller.OnAttackEvent.AddListener(AttackEvent);
        }
    }

    void AttackEvent(Unit thisUnit, DamageReceiver enemyReceiver)
    {
        hitsCounter++;
        if (hitsCounter == requiredHits)
        {
            hero.TryGetComponent(out StatusEffectReceiver effectReceiver);
            
            int ID = GetInstanceID() + effectReceiver.GetInstanceID();
            if(effectReceiver.isDuplicate(ID)) return;
            
            appliedEffect = Instantiate(_bonusDmgeffect, effectReceiver.transform);
            appliedEffect.bonusDmg = Damage[Level];
            appliedEffect.isStackable = false;
            appliedEffect.ID = ID;
            effectReceiver.ApplyStatusEffect(appliedEffect);
        }
        
        if (hitsCounter > requiredHits)
        {
            hitsCounter = 0;
            if(appliedEffect !=null) appliedEffect.RemoveFromReceiver();
            
            if (enemyReceiver.TryGetComponent(out StatusEffectReceiver effectReceiver))
            {
                GiveStun(effectReceiver);
            }
        }
        
    }

    void GiveStun(StatusEffectReceiver effectReceiver)
    {
        int ID = GetInstanceID() + effectReceiver.GetInstanceID();
        if(effectReceiver.isDuplicate(ID)) return;
        
        StunStatusEffect effect = Instantiate(_stun, effectReceiver.transform);
        effect.Duration = stunDur[Level];
        effect.ID = ID;
        
        effectReceiver.ApplyStatusEffect(effect, hero.gameObject);
    }
}
