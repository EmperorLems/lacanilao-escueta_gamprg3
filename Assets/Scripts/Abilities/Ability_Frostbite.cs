using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_Frostbite : AbilityBase
{
 // always leave first item in array for level 0
    [Header("Upgradable Variables")]
    [SerializeField] private float[] HeroDamage = new float[4];
    [SerializeField] private float[] HeroDur = new float[5];
    [SerializeField] private float CreepDamage;
    [SerializeField] private float CreepDur;
    [Header("Prefabs")] [SerializeField] private FrostbiteEffect _frostbiteEffect;
    [SerializeField] private HomingProjectile _projectile;

    private GameObject target;

    protected override void OnActivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.AddListener(OnLeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.RemoveListener(OnLeftClick);
            target = null;
        }
    }

    void OnLeftClick(GameObject obj, Vector3 location)
    {
        if (!CheckIfStillActive()) return;
        if (obj.TryGetComponent(out Unit unit))
        {
            if (unit.faction == hero.faction) return;
            
            if (IsWithinCastRange(unit.transform.position))
            {
                target = obj;
                hero.transform.LookAt(target.transform.position);
                OnFinishTargeting.Invoke(this);
            }
        }
    }

    protected override void OnAbilityAttack()
    {
        HomingProjectile projectile = Instantiate(_projectile, hero.getSpawnPoint().position, 
            hero.gameObject.transform.rotation);
        
        projectile.Setup(target,10,750);
        projectile.OnHit.AddListener(OnProjectileHit);
    }

    private void OnProjectileHit(GameObject mTarget)
    {
        StatusEffectReceiver receiver = mTarget.GetComponent<StatusEffectReceiver>();

        bool isHero = mTarget.GetComponent<Hero>() != null;

        FrostbiteEffect frost = Instantiate(_frostbiteEffect, receiver.transform);
        frost.tick = 0.5f;
        frost.TotalDamage = (isHero) ? HeroDamage[Level] : CreepDamage;
        frost.Duration = (isHero) ? HeroDur[Level] : CreepDur;
        frost.ID = GetInstanceID() + receiver.GetInstanceID();
        
        receiver.ApplyStatusEffect(frost, hero.gameObject);
    }
}
