using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_CoupDeGrace : AbilityBase
{
    [Header("Upgradable Variables")]
    [SerializeField] private float[] criticalDamagePerUpgrade = new float[4];
    
    [Header("Non-Upgradable Variables")]
    private float procChance = 0.15f;
    private CriticalStrike critical;

    private float criticalDamage => criticalDamagePerUpgrade[Level];

    protected override void OnUpgrade()
    {
        if (critical != null)
        {
            critical.damageModifier = criticalDamage;
        }
    }

    protected override void OnActivate()
    {
        critical = new CriticalStrike(procChance, criticalDamage);
        
        hero.CriticalStrikes.Add(critical);
        
    }

    protected override void OnDeactivate()
    {
        hero.CriticalStrikes.Remove(critical);
    }
}
