using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Ability_NetherSwap : AbilityBase
{

    protected override void OnActivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.AddListener(LeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.RemoveListener(LeftClick);
        }
    }

    void LeftClick(GameObject obj, Vector3 location)
    {
        if(!CheckIfStillActive()) return;

        if (obj.layer == 6 || obj.layer == 7 || obj.layer == 10)
        {
            Vector3 prevHeroPos = hero.transform.position;
            Vector3 prevTargetPos = obj.transform.position;

            if (IsWithinCastRange(prevTargetPos))
            {
                hero.transform.position = prevTargetPos;
                obj.transform.position = prevHeroPos;

                hero.state = UnitState.Idle;
                
                Deactivate();
            }
        }
    }
}
