using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TargetsAcquisition))]
public class Ability_SlithereenCrush : AbilityBase
{
    private float[] Damage = new float[5];
    [SerializeField] private float[] moveSpeedSlow = new float[5];
    [SerializeField] private float[] atkSpeedSlow = new float[5];
    [SerializeField] private float[] slowDur = new float[5];
    [SerializeField] private float StunDur;

    private TargetsAcquisition _targetsAcquisition;

    [SerializeField] private StunStatusEffect _stun;
    [SerializeField] private SlowStatusEffect _slow;
    [SerializeField] private AttackSpeedBonusEffect _atkSpd;


    protected override void OnSetup()
    {
        _targetsAcquisition = GetComponent<TargetsAcquisition>();
        _targetsAcquisition.Setup(hero.transform,hero.faction,false);
    }

    protected override void OnActivate()
    {
        OnFinishTargeting.Invoke(this);
    }

    protected override void OnAbilityAttack()
    {
        foreach (Unit enemyUnit in _targetsAcquisition.GetNearbyEnemyUnits(CastRange/5))
        {
            if (enemyUnit.TryGetComponent(out StatusEffectReceiver effectReceiver))
            {
                int ID = GetInstanceID() + effectReceiver.GetInstanceID();

                if (!effectReceiver.isDuplicate(ID))
                {
                    StunStatusEffect se = Instantiate(_stun, effectReceiver.transform);
                    se.Duration = StunDur;
                    se.ID = ID;

                    se.OnEffectEnd.AddListener(OnStunEnd);
                    effectReceiver.ApplyStatusEffect(se);
                }
            }

            if (enemyUnit.TryGetComponent(out DamageReceiver dmgReceiver))
            {
                dmgReceiver.TakeDamage(Damage[Level], AttackType.Hero, hero.gameObject);
            }
        }
    }

    void OnStunEnd(StatusEffect effect, StatusEffectReceiver receiver)
    {
        int effectID = GetInstanceID() + receiver.GetInstanceID();
        SlowStatusEffect slowStatusEffect = Instantiate(_slow, receiver.transform);
        slowStatusEffect.Duration = slowDur[Level];
        slowStatusEffect.speedMod = moveSpeedSlow[Level];
        slowStatusEffect.ID = effectID;
        receiver.ApplyStatusEffect(slowStatusEffect);

        AttackSpeedBonusEffect atkSpeed = Instantiate(_atkSpd, receiver.transform);
        atkSpeed.Duration = slowDur[Level];
        atkSpeed.speedMod = -atkSpeedSlow[Level];
        atkSpeed.ID = effectID;
        
        receiver.ApplyStatusEffect(atkSpeed);
    }
}
