using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_PhantomStrike : AbilityBase
{
    [Header("Prefabs")]
    [SerializeField] private AttackSpeedBonusEffect effect;
    [Header("Upgradable Variables")]
    [SerializeField] private float[] atkSpeedBonusPerUpgrade = new float[5];
    [Header("Non-Upgradable Variables")]
    [SerializeField] private float atkSpeedBonusDuration = 2;

    private float currentAtkSpeedBonus => atkSpeedBonusPerUpgrade[Level];
    

    protected override void OnActivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.AddListener(LeftClick);
        }
    }

    protected override void OnDeactivate()
    {
        if (hero.GetComponent<PlayerController>() != null)
        {
            PlayerController pc = hero.GetComponent<PlayerController>();
            pc.OnLeftClickEvent.RemoveListener(LeftClick);
        }
    }

    void LeftClick(GameObject obj, Vector3 location)
    {
        if(!CheckIfStillActive()) return;

        if (obj.TryGetComponent(out Unit unit))
        {
            Vector3 targetPos = unit.transform.forward * -1.5f;

            if (IsWithinCastRange(targetPos))
            {
                hero.transform.position = targetPos;

                if (unit.faction != hero.faction)
                {
                    OnEnemyTarget(unit);
                }
                Deactivate();
            }
        }
    }

    void OnEnemyTarget(Unit enemy)
    {
        StatusEffectReceiver receiver = hero.GetComponent<StatusEffectReceiver>();

        int ID = GetInstanceID() + receiver.GetInstanceID();
        if(receiver.isDuplicate(ID)) return;
        
        AttackSpeedBonusEffect se = Instantiate(effect, hero.transform);
        se.speedMod = currentAtkSpeedBonus;
        se.Duration = atkSpeedBonusDuration;
        se.ID = ID;
        
        receiver.ApplyStatusEffect(se);
        if (hero.TryGetComponent(out HeroController controller))
        {
            controller.SetNewTarget(enemy.gameObject);
        }
    }
}
