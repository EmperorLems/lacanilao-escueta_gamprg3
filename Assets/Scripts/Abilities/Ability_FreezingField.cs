using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(TargetsAcquisition))]
public class Ability_FreezingField : AbilityBase
{
    [SerializeField] private float minSpawnRadius, maxSpawnRadius, dmgRadius, armorBonus, moveSpeedSlow, atkSpdSlow, slowDur;
    [SerializeField] private BonusArmor _bonusArmor;
    [SerializeField] private SlowStatusEffect _slow;
    [SerializeField] private AttackSpeedBonusEffect _atkspd;
    [SerializeField] private Explosion _explosion;

    private TargetsAcquisition _targetsAcquisition;
    protected override void OnSetup()
    {
        _targetsAcquisition = GetComponent<TargetsAcquisition>();
        _targetsAcquisition.Setup(hero.transform,hero.faction,false);
    }

    protected override void OnActivate()
    {
        //StartCoroutine(RoutineWithDuration(10, 0.1f));
        StartCoroutine(ChannellingRoutine(10));

        hero.TryGetComponent(out StatusEffectReceiver receiver);

        BonusArmor bonusArmor = Instantiate(_bonusArmor, receiver.transform);
        bonusArmor.armorBonus = armorBonus;
        bonusArmor.ID = _bonusArmor.ID + GetInstanceID() + receiver.GetInstanceID();
        receiver.ApplyStatusEffect(bonusArmor);
    }

    protected override void OnDeactivate()
    {
        StopCoroutine(RoutineWithDuration(10,0.5f));
        hero.TryGetComponent(out StatusEffectReceiver receiver);
        receiver.RemoveStatusEffect(_bonusArmor.ID + GetInstanceID() + receiver.GetInstanceID());
    }

    protected override void OnAbilityAttack()
    {
        StartCoroutine(RoutineWithDuration(10, 0.1f));
        hero.TryGetComponent(out StatusEffectReceiver receiver);
        receiver.RemoveStatusEffect(_bonusArmor.ID + GetInstanceID() + receiver.GetInstanceID());
    }

    protected override void OnTick()
    {
        SpawnExplosion();
        foreach (Unit unit in _targetsAcquisition.GetNearbyEnemyUnits(CastRange/10))
        {
            if (unit.TryGetComponent(out StatusEffectReceiver effectReceiver))
            {
                int moveID = _slow.GetInstanceID() + GetInstanceID() + effectReceiver.GetInstanceID();
                int atkID = _atkspd.GetInstanceID() + GetInstanceID() + effectReceiver.GetInstanceID();

                if (!effectReceiver.isDuplicate(moveID) &&
                    !effectReceiver.isDuplicate(atkID))
                {
                    GiveEffect(effectReceiver);
                }
            }
        }
    }

    void SpawnExplosion()
    {
        Vector3 pos = transform.position;
        pos += Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up) * Vector3.forward * Random.Range(minSpawnRadius/10,40);
        
        Explosion explosion = Instantiate(_explosion);
        explosion.Setup(pos,new Vector3(dmgRadius/10,dmgRadius/10,dmgRadius/10), 1f);
        explosion.OnHit.AddListener(obj =>
        {
            if (obj.TryGetComponent(out Unit unit))
            {
                if(unit.faction == hero.faction) return;
            }
            if (obj.TryGetComponent(out StatusEffectReceiver effectReceiver))
            {
                int moveID = _slow.GetInstanceID() + GetInstanceID() + effectReceiver.GetInstanceID();
                int atkID = _atkspd.GetInstanceID() + GetInstanceID() + effectReceiver.GetInstanceID();
                
                effectReceiver.RemoveStatusEffect(moveID);
                effectReceiver.RemoveStatusEffect(atkID);
                
                GiveEffect(effectReceiver);
            }

            if (obj.TryGetComponent(out DamageReceiver damageReceiver))
            {
                damageReceiver.TakeDamage(hero.Damage, AttackType.Hero, hero.gameObject, true);
            }
        });
    }

    void GiveEffect(StatusEffectReceiver effectReceiver)
    {
        SlowStatusEffect SE = Instantiate(_slow, effectReceiver.transform);
        SE.speedMod = 1 - moveSpeedSlow;
        SE.Duration = slowDur;
        SE.ID =_slow.GetInstanceID() + GetInstanceID() + effectReceiver.GetInstanceID();
        
        effectReceiver.ApplyStatusEffect(SE);
        
        AttackSpeedBonusEffect asbe = Instantiate(_atkspd, effectReceiver.transform);
        asbe.speedMod = -atkSpdSlow;
        asbe.Duration = slowDur;
        asbe.ID = _atkspd.GetInstanceID() + GetInstanceID() + effectReceiver.GetInstanceID();
        
        effectReceiver.ApplyStatusEffect(asbe);
    }
}
