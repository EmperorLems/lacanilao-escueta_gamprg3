using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(TargetsAcquisition))]
public class Ability_HeartstopperAura : AbilityBase
{
    [Header("Upgradable Variables")] [SerializeField] 
    private float[] MaxHPLostPerUpagrde = new float[5];

    [SerializeField] private float[] RegenBonus = new float[5];

    private TargetsAcquisition _targetsAcquisition;

    [Header("Prefabs")] 
    [SerializeField] private HeartStopperEffect _stopperEffect;
    [SerializeField] private RegenBonusEffect _regenBonusEffect;

    private List<HeartStopperEffect> _heartStopperApplied = new List<HeartStopperEffect>();
    private float stackDuration = 7;
    protected override void OnSetup()
    {
        _targetsAcquisition = GetComponent<TargetsAcquisition>();
        _targetsAcquisition.Setup(hero.transform, hero.faction, false, 0.1f, CastRange / 5, false);
    }

    protected override void OnActivate()
    {
        //_targetsAcquisition.Setup(hero.transform, hero.faction, true, 0.1f, CastRange / 5, false);
        StartCoroutine(PassiveRoutine(0.1f));
    }

    protected override void OnTick()
    {
        foreach (Unit unit in _targetsAcquisition.GetNearbyEnemyUnits(CastRange/5))
        {
            if (unit.TryGetComponent(out StatusEffectReceiver seReceiver))
            {
                GiveHeartStopper(seReceiver);
            }
        }

        // remove all null bonus armor effect
        _heartStopperApplied = _heartStopperApplied.Where(aa => aa != null).ToList();
            
        // remove all effects that are far from hero
        List<HeartStopperEffect> effectsToRemove = _heartStopperApplied.Where(aa =>
            Vector3.Distance(transform.position, aa.transform.position) >= 20).ToList();

        foreach (HeartStopperEffect _hsEffect in effectsToRemove)
        {
            RemoveHeartStopper(_hsEffect);
        }
    }

    void GiveHeartStopper(StatusEffectReceiver receiver)
    {
        int ID = GetInstanceID() + receiver.GetInstanceID();
        if (!receiver.isDuplicate(ID))
        {
            HeartStopperEffect effect = Instantiate(_stopperEffect, receiver.transform);
            effect.tick = 0.2f;
            effect.Necrophos = hero.gameObject;
            effect.MaxHpPercentLost = MaxHPLostPerUpagrde[Level];
            effect.ID = ID;
            effect.tick = .2f;
            effect.Duration = 10;
            receiver.ApplyStatusEffect(effect , hero.gameObject);
            _heartStopperApplied.Add(effect);

            effect.OnNecrophosKills += OnKill;
        }
    }

    void RemoveHeartStopper(HeartStopperEffect effect)
    {
        _heartStopperApplied.Remove(effect);
        effect.RemoveFromReceiver();
    }

    void OnKill(bool isKilledHero)
    {
        if (hero.TryGetComponent(out StatusEffectReceiver receiver))
        {
            int stacks = (isKilledHero) ? 6 : 1;

            for (int i = 0; i < stacks; i++)
            {
                RegenBonusEffect bonusEffect = Instantiate(_regenBonusEffect, receiver.transform);
                bonusEffect.HpRegenBonus = RegenBonus[Level];
                bonusEffect.ManaRegenBonus = RegenBonus[Level];
                bonusEffect.ID = receiver.GetInstanceID() + GetInstanceID();
                bonusEffect.isStackable = true;
                bonusEffect.Duration = stackDuration;
                receiver.ApplyStatusEffect(bonusEffect);
            }
        }
    }
}
