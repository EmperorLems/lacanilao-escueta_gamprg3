using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_Blur : AbilityBase
{
    [Header("Prefabs")]
    [SerializeField] private EvasionBonusEffect _evasionPrefab;
    [SerializeField] private AttackSpeedBonusEffect _atkSpdEffectPrefab;
    
    [Header("Upgradable Variables")]
    [SerializeField] private float[] evasionPerUpgrade = new float[5];
    [SerializeField] private float[] atkSpdPerUpgrade = new float[5];
    
    [Header("Non-Upgradable Variables")]
    [SerializeField] private float atkSpdDuration;
    private float currentEvasion => evasionPerUpgrade[Level];
    private float currentAtkSpdPerStack => atkSpdPerUpgrade[Level];
    private EvasionBonusEffect appliedEvasion;

    protected override void OnUpgrade()
    {
        if (appliedEvasion != null)
        {
            appliedEvasion.UpdateEvasion(currentEvasion);
        }
    }
    
    protected override void OnActivate()
    {
        if (hero.TryGetComponent(out StatusEffectReceiver receiver))
        {
            appliedEvasion = Instantiate(_evasionPrefab, receiver.transform);
            appliedEvasion.ID = this.GetInstanceID() + receiver.GetInstanceID();
            
            receiver.ApplyStatusEffect(appliedEvasion);
            appliedEvasion.UpdateEvasion(currentEvasion);
        }

        if (hero.TryGetComponent(out DamageReceiver dmgReceiver))
        {
            dmgReceiver.OnEvade += OnEvaded;
        }
    }

    void OnEvaded(GameObject attacker)
    {
        StatusEffectReceiver receiver = hero.GetComponent<StatusEffectReceiver>();

        AttackSpeedBonusEffect se = Instantiate(_atkSpdEffectPrefab, hero.transform);
        se.speedMod = 100;//currentAtkSpdPerStack;
        se.Duration = atkSpdDuration;
        se.isStackable = true;
        se.ID = receiver.GetInstanceID() + this.GetInstanceID();
        
        receiver.ApplyStatusEffect(se, this.gameObject);
    }
}
