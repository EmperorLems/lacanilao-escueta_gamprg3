using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class UnitManager : MonoBehaviour
{
    [SerializeField] private Transform DireSpawnPoint;
    [SerializeField] private Transform RadiantSpawnPoint;
    [SerializeField] private LayerMask layers;
    
    private List<Creeps> direCreeps = new List<Creeps>();
    private List<Creeps> radiantCreeps = new List<Creeps>();
    
    private List<Hero> direHeroes = new List<Hero>();
    private List<Hero> radiantHeroes = new List<Hero>();

    private List<Tower> direTowers = new List<Tower>();
    private List<Tower> radiantTowers = new List<Tower>();

    public Action<Hero> OnWaitingForRespawn;
    void Start()
    {
        // Hero[] Heroes = FindObjectsOfType<Hero>();
        //
        // foreach (Hero h in Heroes)
        // {
        //     AddHeroToList(h);
        // }

        Tower[] towers = FindObjectsOfType<Tower>();

        foreach (Tower t in towers)
        {
            AddTowerToList(t);
        }
    }

    #region OnDeath Listeners

    void OnCreepDeath(Unit owner, GameObject attacker)
    {
        Creeps deadCreep = owner as Creeps;
        if (deadCreep == null) return;
        
        // remove from list
        if (deadCreep.faction == Faction.Dire) direCreeps.Remove(deadCreep);
        else radiantCreeps.Remove(deadCreep);
        
        // get all nearby Heroes
        List<Hero> nearbyHeroes = new List<Hero>();
        nearbyHeroes = GetNearbyEnemyHeroes(deadCreep.transform.position, deadCreep.faction);
        
        //early return if there is no nearby heroes
        if(nearbyHeroes.Count==0) return;
        
        // calculate xp to give each hero
        int XPDrop = (deadCreep.attackType == AttackType.Pierce)
            ? Mathf.RoundToInt(deadCreep.XpBounty / nearbyHeroes.Count)
            : 0;
        
        // give gold to attacker
        if (attacker.layer == 10)
        {
            attacker.GetComponent<Hero>().GetInventory().AddGold(deadCreep.goldBounty);
        }

        // give gold and XP to nearby heroes
        foreach (Hero hero in nearbyHeroes)
        {
            if (hero.gameObject != attacker)
            {
                hero.GetInventory().AddGold( Mathf.RoundToInt(deadCreep.goldBounty * 0.25f) );
            }
            hero.GetLevelComponent().AddXP(XPDrop);
        }
    }

    void OnHeroDeath(Unit owner, GameObject attacker)
    {
        Hero deadHero = owner as Hero;
        if (deadHero == null) return;
        
        // get all nearby Heroes
        List<Hero> nearbyHeroes = new List<Hero>();
        nearbyHeroes = GetNearbyEnemyHeroes(deadHero.transform.position, deadHero.faction);
        
        if (nearbyHeroes.Count > 0)
        {
            // calculate xp to give each hero
            int XPDrop = Mathf.RoundToInt(deadHero.GetLevelComponent().KillXpReward / nearbyHeroes.Count);
        
            // give xp
            foreach (Hero hero in nearbyHeroes)
            {
                hero.GetLevelComponent().AddXP(XPDrop);
            }
        }
        
        //give gold to killer
        if (attacker.TryGetComponent(out Hero killer))
        {
            killer.GetInventory().AddGold(deadHero.GetLevelComponent().GoldLastHitBounty);
        }
        
        UpdateScoreBoard(deadHero,attacker);
        
        //Respawn
        deadHero.getController().OnFinishDeathAnimation.AddListener(heroUnit =>
        {
            StartCoroutine(Respawn(heroUnit as Hero));
        });
    }

    void OnTowerDestroy(Tower owner, GameObject attacker)
    {
        Tower destroyedTower = owner.GetComponent<Tower>();

        if (attacker.GetComponent<InventoryComponent>() != null)
        {
            attacker.GetComponent<InventoryComponent>().AddGold(200);
        }

        List<Hero> heroes = new List<Hero>();

        heroes = (destroyedTower.faction == Faction.Radiant) ? direHeroes : radiantHeroes;

        foreach (Hero h in heroes)
        {
            if (h.gameObject.activeSelf)
            {
                h.GetInventory().AddGold(destroyedTower.GoldBounty);
            }
        }
    }
    
    #endregion

    #region Add To List Functions
    
    public void AddCreepToList(Creeps creep)
    {
        if (creep.faction == Faction.Dire) direCreeps.Add(creep);
        else radiantCreeps.Add(creep);
        
        creep.OnUnitDeath.AddListener(OnCreepDeath);
    }

    public void AddHeroToList(Hero hero)
    {
        if (hero.faction == Faction.Dire) direHeroes.Add(hero);
        else radiantHeroes.Add(hero);

        hero.OnUnitDeath.AddListener(OnHeroDeath);
        GameManager.instance.getScoreBoard().AddHeroToScoreBoard(hero);
    }

    public void AddTowerToList(Tower tower)
    {
        if (tower.faction == Faction.Dire) direTowers.Add(tower);
        else radiantTowers.Add(tower);

        tower.OnTowerDestroyed.AddListener(OnTowerDestroy);
    }
    
    #endregion

    public void ScaleCreeps()
    {
        List<Creeps> creepsList = new List<Creeps>();
        creepsList.AddRange(direCreeps);
        creepsList.AddRange(radiantCreeps);

        foreach (Creeps c in creepsList)
        {
            UpgradeCreep(c);
        }
    }

    #region Private Functions

    private void UpgradeCreep(Creeps creep)
    {
        switch (creep.attackType)
        {
            case AttackType.Basic:
                creep.GetHealthComponent().SetMaxHealthWithHeal(creep.GetHealthComponent().GetMaxHealth() + 12);
                creep.Damage += 1;
                creep.goldBounty += 1;
                break;
            
            case AttackType.Pierce:
                creep.GetHealthComponent().SetMaxHealthWithHeal(creep.GetHealthComponent().GetMaxHealth() + 12);
                creep.Damage += 2;
                creep.goldBounty += 6;
                creep.XpBounty += 8;
                break;
        }
    }

    private IEnumerator Respawn(Hero hero)
    {
        hero.state = UnitState.Die;
        
        hero.transform.position = (hero.faction == Faction.Dire) ? DireSpawnPoint.position : RadiantSpawnPoint.position;
        hero.gameObject.SetActive(false);

        hero.RespawnTimer = hero.GetLevelComponent().RespawnTime;
        OnWaitingForRespawn.Invoke(hero);
        while (hero.RespawnTimer > 0)
        {
            yield return new WaitForSeconds(1);
            hero.RespawnTimer--;
        }
        

        hero.gameObject.SetActive(true);
        hero.GetHealthComponent().HealHP(999999);
        hero.state = UnitState.Idle;
        
        yield return new WaitForSeconds(1);
        hero.state = UnitState.Follow;
    }
    
    private List<Hero> GetNearbyEnemyHeroes(Vector3 location , Faction faction)
    {
        List<Hero> nearbyEnemyHeroes = new List<Hero>();

        Collider[] colliders = Physics.OverlapSphere(location, 45, layers);

        foreach (Collider c in colliders)
        {
            if (c.gameObject.layer == 10)
            {
                if (c.gameObject.GetComponent<Hero>().faction != faction)
                {
                    nearbyEnemyHeroes.Add(c.GetComponent<Hero>());
                }
            }
        }
        
        return nearbyEnemyHeroes;
    }

    private void UpdateScoreBoard(Hero deadHero, GameObject attacker)
    {
        if (attacker.TryGetComponent(out ScoreManager killer))
        {
            killer.Kills++;
            if (killer.TryGetComponent(out Unit unit))
            {
                if (unit.faction == Faction.Dire) GameManager.instance.direScore++;
                else GameManager.instance.radiantScore++;
            }
        }

        if (deadHero.TryGetComponent(out ScoreManager dead))
        {
            dead.Deaths++;
        }
    }
    #endregion

    public List<Hero> DireHeroes => direHeroes;
    public List<Hero> RadiantHeroes => radiantHeroes;
}
