using UnityEngine;

[RequireComponent(typeof(CreepController))]
public class Creeps : Unit
{
    public int goldBounty;
    public int XpBounty;

    private void Start()
    {
        GetHealthComponent().OnDeath.AddListener(OnDeath);
        InitUnit();
    }
    

    #region Overrides

    #endregion

}
