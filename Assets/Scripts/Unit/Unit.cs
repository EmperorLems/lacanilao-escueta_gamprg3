using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public enum Faction
{
    Radiant,
    Dire
}

public enum UnitState
{
    Idle,
    Chase,
    Attack,
    Channeling,
    SpecialAttack,
    Follow,
    Die,
}

[RequireComponent(typeof(DamageReceiver))]
[RequireComponent(typeof(StatusEffectReceiver))]
[RequireComponent(typeof(NavMeshAgent))]
public class Unit : MonoBehaviour
{
    [SerializeField] protected UnitData unitData;
    [SerializeField] private HealthComponent healthComponent;
    private UnitController controller;
    public Transform projectileSpawnPoint;
    
    private string unitName;
    private float healthRegen;
    private float manaRegen;
    private float moveSpeed;
    private float baseDamage;
    private float baseArmor;
    private float bonusArmor;
    private float rawMagicResist = 1;
    private float attackSpeed;
    private float attackTime;
    private float attackRange;
    private float rawEvasion = 1;

    private bool isPhysicalImmune;
    public Faction faction;
    public UnitState state;

    public bool isTakingDamage = false;

    [HideInInspector] public UnityEvent<Unit, GameObject> OnUnitDeath;
    private List<CriticalStrike> criticalStrikes = new List<CriticalStrike>();
    protected void InitUnit()
    {
        controller = GetComponent<UnitController>();
        healthComponent.SetMaxHealthWithHeal(unitData.maxHealth);

        unitName = unitData.name;
        healthRegen = unitData.healthRegen;
        manaRegen = unitData.manaRegen;
        baseArmor = unitData.baseArmor;
        StackMagicResist(unitData.magicResist);
        MoveSpeed = unitData.moveSpeed;
        baseDamage = unitData.baseDamage;
        attackSpeed = unitData.attackSpeed;
        attackTime = unitData.attackTime;
        attackRange = unitData.attackRange;

        bonusArmor = 0;
    }

    #region getters

    // public virtual string GetUnitName() => unitName;
    public virtual HealthComponent GetHealthComponent() => healthComponent;

    public UnitController getController() => controller;

    public virtual UnitData GetUnitData() => unitData;

    public AttackType attackType => unitData.attackType;
    
    public ArmorData armorData => unitData.armorData;

    public List<CriticalStrike> CriticalStrikes => criticalStrikes;
    
    public float getAttackInterval()
    {        
        float attacksPerSecond = attackSpeed / (100 * attackTime);
        float interval = 1 / attacksPerSecond;

        return interval;
    }
    #endregion

    #region getters and setters

    public string UnitName => unitName;

    public virtual float HealthRegen
    {
        get => healthRegen;
        set => healthRegen = value;
    }

    public virtual float ManaRegen
    {
        get => manaRegen;
        set => manaRegen = value;
    }

    public virtual float Armor
    {
        get => baseArmor;
        set => baseArmor = value;
    }

    public virtual float BonusArmor => bonusArmor;

    public virtual float TotalArmor => Armor + BonusArmor;
    public virtual float MagicResist => 1 - rawMagicResist;

    public virtual float MoveSpeed
    {
        get => moveSpeed;
        set
        {
            moveSpeed = value;
            if (moveSpeed > 550) moveSpeed = 550;
            controller.SetMovementSpeed(moveSpeed);
        }
    }

    public virtual float Damage
    {
        get => baseDamage;
        set => baseDamage = value;
    }

    public virtual float AttackSpeed
    {
        get => attackSpeed;
        set => attackSpeed = value;
    }

    public virtual float AttackTime
    {
        get => attackTime;
        set => attackTime = value;
    }

    public virtual float AttackRange
    {
        get => attackRange;
        set => attackRange = value;
    }

    public virtual float Evasion => 1 - rawEvasion;

    public bool IsPhysicalImmune
    {
        get => isPhysicalImmune;
        set => isPhysicalImmune = value;
    }

    #endregion

    #region Stacking Stats

    public void StackMagicResist(float resistVal)
    {
        rawMagicResist *= (1 - resistVal);
    }

    public void RemoveMagicResistStack(float resistVal)
    {
        rawMagicResist /= (1 - resistVal);
    }

    public void StackEvasion(float evasionVal)
    {
        rawEvasion *= (1 - evasionVal);
    }

    public void RemoveEvasionStack(float evasionVal)
    {
        rawEvasion /= (1 - evasionVal);
    }
    #endregion
    #region PublicFunctions
    

    //flat value regen
    protected virtual void RegenerateHealth()
    {
        if (!GetHealthComponent().IsMaxHealth())
        {
            GetHealthComponent().HealHP(HealthRegen);
        }
    }
    
    
    protected void OnDeath(GameObject attacker)
    {
        state = UnitState.Die;
        OnDeathActions();
        OnUnitDeath.Invoke(this, attacker);
    }

    protected virtual void OnDeathActions(){}
    public virtual void AddBonusArmor(float armorVal)
    {
        bonusArmor += armorVal;
    }

    public virtual void RemoveBonusArmor(float armorVal)
    {
        bonusArmor -= armorVal;
    }
    #endregion
}