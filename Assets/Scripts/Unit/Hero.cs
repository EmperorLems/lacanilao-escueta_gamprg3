using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class HeroStats
{
    public float str;
    public float agi;
    public float _int;
    public int primary;
}

[RequireComponent(typeof(AbilityUser))]
[RequireComponent(typeof(InventoryComponent))]
[RequireComponent(typeof(LevelingComponent))]
[RequireComponent(typeof(ScoreManager))]
public class Hero : Unit
{
    [SerializeField] private ManaComponent manaComponent;
    [SerializeField] private InventoryComponent inventory;
    [SerializeField] private LevelingComponent levelComponent;
    [SerializeField] private AbilityUser _abilityUser;
    private HeroStats heroStats = new HeroStats();

    private float respawnTimer = 0;
    public Collider attackZone;
    private delegate float GetPrimaryStat();

    private GetPrimaryStat _primaryStat;

    private void Awake()
    {
        InitUnit();
        
        heroStats.str = unitData.stats.str;
        heroStats.agi = unitData.stats.agi;
        heroStats._int = unitData.stats._int;
        heroStats.primary = unitData.stats.primary;
        
        switch (heroStats.primary)
        {
            case 0:
                _primaryStat = () => heroStats.str;
                break;
            case 1:
                _primaryStat = () => heroStats.agi;
                break;
            case 2:
                _primaryStat = () => heroStats._int;
                break;
            default:
                _primaryStat = () => 0;
                break;
        }
    }

    private void Start()
    {
        UpdateStat();
        
        GetHealthComponent().OnDeath.AddListener(OnDeath);

        StartCoroutine(RegenHpAndMana(0.1f));
    }

    #region Component Getters
    
    public HeroStats GetHeroStats() => heroStats;

    public ManaComponent GetManaComponent() => manaComponent;

    public InventoryComponent GetInventory() => inventory;

    public LevelingComponent GetLevelComponent() => levelComponent;
    public Transform getSpawnPoint() => projectileSpawnPoint;

    public AbilityUser getAbilityUser() => _abilityUser;


    public float RespawnTimer
    {
        get => respawnTimer;
        set => respawnTimer = value;
    }

    #endregion

    private void RegenerateMana()
    {
        if (!manaComponent.IsMaxMana())
        {
            manaComponent.AddMana(ManaRegen);
        }
    }

    IEnumerator RegenHpAndMana(float tick)
    {
        while (gameObject != null)
        {
            if (state != UnitState.Die)
            {
                if (!GetHealthComponent().IsMaxHealth() && GetHealthComponent().GetCurrentHealth() > 0)
                {
                    GetHealthComponent().HealHP(HealthRegen * tick);
                }
                
                if (!manaComponent.IsMaxMana())
                {
                    manaComponent.AddMana(ManaRegen * tick);
                }
            }

            yield return new WaitForSeconds(tick);
        }
    }
    
    public void UpdateStat()
    {
        float _hp = unitData.maxHealth + (heroStats.str * 20);
        GetHealthComponent().SetMaxHealthWithHeal(_hp);

        float _mana = unitData.maxMana + (heroStats._int * 12);
        manaComponent.SetMaxMana(_mana);

        float _damage = Damage + _primaryStat();
        Damage += _primaryStat();
        
        float _atkSpd = AttackSpeed + heroStats.agi;
        AttackSpeed += heroStats.agi;
        
        float _HpRegen = HealthRegen + (heroStats.str * 0.1f);
        HealthRegen = HealthRegen;
        
        float _manaRegen = ManaRegen + (heroStats._int * 0.05f);
        ManaRegen = _manaRegen;
        
        float _armor = Armor + (heroStats.agi * 0.16f);
        Armor = _armor;
    }

    public void OnStatGrow()
    {
        heroStats.str += unitData.growth.str;
        heroStats.agi += unitData.growth.agi;
        heroStats._int += unitData.growth._int;

        UpdateStat();
    }

    public void AddHeroStats(HeroStats stats)
    {
        heroStats.str += stats.str;
        heroStats.agi += stats.agi;
        heroStats._int += stats._int;
        
        UpdateStat();
    }

    public float MaxHealth
    {
        get => GetHealthComponent().GetMaxHealth();
        set
        {
            GetHealthComponent().SetMaxHealthWithHeal(value);
        }
    }

    public float CurrentHealth
    {
        get => GetHealthComponent().GetCurrentHealth();
    }

    public float MaxMana
    {
        get => GetManaComponent().GetMaxMana();
        set
        {
            GetManaComponent().SetMaxMana(value);
        }
    }
    
    public float CurrentMana => GetManaComponent().GetCurrentMana();

}
