using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Tower : MonoBehaviour
{
    [SerializeField] private HomingProjectile projectile;
    [SerializeField] private HealthComponent healthComponent;
    [SerializeField] private GameObject SpawnPoint;
    [SerializeField] private TowerData towerData;
    [SerializeField] private BonusArmor bonusArmor;
    [SerializeField] private TargetsAcquisition targetAcquisition;
    [SerializeField] private float attackSpeed;
    [SerializeField] private float attackTime;
    [SerializeField] private List<Tower> nextTowers;
    
    public UnityEvent<Tower, GameObject> OnTowerDestroyed;
    public Faction faction;
    public bool invulnerable;
    public TowerData TowerData => towerData;
    
    private float armor;
    private Unit currentTarget;
    private IEnumerator attackCoroutine;
    private List<BonusArmor> armorsApplied = new List<BonusArmor>();


    private void Awake()
    {
        Setup();
    }

    void Start()
    {
        attackCoroutine = Attack();
        healthComponent = GetComponentInChildren<HealthComponent>();
        healthComponent.OnDeath.AddListener(TowerDestroy);

        targetAcquisition = GetComponent<TargetsAcquisition>();
        
        if (targetAcquisition != null)
        {
            targetAcquisition.OnNewTarget.AddListener(OnTargetEnter);
            targetAcquisition.OnNoEnemyNearby.AddListener(OnNoTarget);
            
            targetAcquisition.Setup(gameObject.transform, faction,true, 0.1f, 30.0f, false);

            StartCoroutine(ArmorProtection());
            StartCoroutine(Attack());
        }
        
        GetComponent<DamageReceiver>().OnDamaged.AddListener(attacker =>
        {
            if(invulnerable) healthComponent.SetMaxHealthWithHeal(towerData.health);
        });
    }

    private void Setup()
    {
        healthComponent.SetMaxHealthWithHeal(towerData.health);
        armor = towerData.armor;
    }
    

    #region Getters and Setters
    
    public float Armor
    {
        get => armor;
        set => armor = value;
    }

    public float GetAttackInterval()
    {
        float attacksPerSecond = towerData.attackSpeed / (100 * attackTime);

        float interval = 1 / attacksPerSecond;

        return interval;
    }

    public HealthComponent GetHealthComponent() => healthComponent;

    public int GoldBounty => towerData.GoldBounty;
    
    #endregion
    
    public void SetSpawnPoint(GameObject spawnPoint)
    {
        SpawnPoint = spawnPoint;
    }

    IEnumerator Attack()
    {
        while (this.gameObject != null)
        {
            if (currentTarget != null)
            {
                //isAttacking = true;
            
                // Spawn Projectile 
                Instantiate(projectile, SpawnPoint.transform.position, Quaternion.identity)
                    .Setup(currentTarget.gameObject, towerData.damage, this.gameObject, AttackType.Siege);
                //isAttacking = false;
            }
            yield return new WaitForSeconds(GetAttackInterval());
        }
    }

    private void TowerDestroy(GameObject attacker)
    {
        if (nextTowers.Count > 0)
        {
            foreach (Tower tower in nextTowers)
            {
                tower.invulnerable = false;
            }
        }
        else
        {
            Faction playerFaction = GameObject.FindObjectOfType<PlayerController>().GetComponent<Hero>().faction;
            GameManager.instance.GameOver(this.faction != playerFaction);
        }
        OnTowerDestroyed.Invoke(this, attacker);
        Destroy(this.gameObject);
    }

    private void OnTargetEnter(Unit unit)
    {
        currentTarget = unit;
    }

    private void OnNoTarget(TargetsAcquisition targetsAcquisition)
    {
        currentTarget = null;
    }

    IEnumerator ArmorProtection()
    {
        while (!healthComponent.IsHealthDepleted())
        {
            yield return new WaitForSeconds(1);
            
            foreach (Unit unit in targetAcquisition.GetNearbyFriendlyUnits(30))
            {
                StatusEffectReceiver receiver = unit.GetComponent<StatusEffectReceiver>();

                Assert.IsNotNull(receiver, "receiver is null");

                int ID = this.GetInstanceID() + receiver.GetInstanceID() + bonusArmor.GetInstanceID();

                // if it doesn't have bonus armor from this tower, apply bonus armor and add to list 
                if (!receiver.isDuplicate(ID))
                {
                    BonusArmor effect = Instantiate(bonusArmor, unit.transform);
                    effect.armorBonus = towerData.armorProtectionBonus;
                    effect.ID = ID;
                    receiver.ApplyStatusEffect(effect, this.gameObject);

                    armorsApplied.Add(effect);
                }
            }
            
            // remove all null bonus armor effect
            armorsApplied = armorsApplied.Where(aa => aa != null).ToList();
            
            // remove all effects that are far from tower
            List<BonusArmor> armorsToRemove = armorsApplied.Where(aa =>
                Vector3.Distance(transform.position, aa.transform.position) >= 30).ToList();

            foreach (BonusArmor ba in armorsToRemove)
            {
                ba.RemoveFromReceiver();
                armorsApplied.Remove(ba);
            }
        }
    }
}
