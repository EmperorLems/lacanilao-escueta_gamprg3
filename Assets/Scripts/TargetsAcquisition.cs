using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TargetsAcquisition : MonoBehaviour
{
    [SerializeField] private Transform owner;
    private List<Unit> unitList = new List<Unit>();
    private List<Tower> towerList = new List<Tower>();

    private Unit currentUnit;
    private Tower currentTower;
    
    private float tick = 0.1f;
    public float radius = 30;
    private bool includeTowers = true;

    private Faction faction;

    [HideInInspector] public UnityEvent<Unit> OnNewTarget;
    [HideInInspector] public UnityEvent<TargetsAcquisition> OnNoEnemyNearby;
    
    private LayerMask layer;
    public Unit targetUnit => currentUnit;
    public Tower targetTower => currentTower;

    public List<Unit> nearbyUnits=> unitList;
    public List<Tower> nearbyTowers => towerList;

    private bool mIsLooping = false, alreadySetup = false;
    public bool Looping => mIsLooping;
    public bool AlreadySetup => alreadySetup;
    // call this to start Coroutine
    public void Setup(Transform mOnwer, Faction mFaction, bool isLooping = true, float mTick = 0.1f, float mRadius = 30, bool mIncludeTowers = true)
    {
        layer = GameManager.instance.UnitsTowersLayer;
        owner = mOnwer;
        faction = mFaction;
        tick = mTick;
        radius = mRadius;
        mIsLooping = isLooping;
        if (isLooping)
        {
            StartCoroutine(SphereCastingRoutine());
        }

        alreadySetup = true;
    }

    private void OnDrawGizmos()
    {
        //Gizmos.DrawSphere(owner.position, radius);
    }

    private void OnEnable()
    {
        if (alreadySetup && mIsLooping)
        {
            StartCoroutine(SphereCastingRoutine());
        }
    }

    private void OnDisable()
    {
        if (alreadySetup && mIsLooping)
        {
            StopCoroutine(SphereCastingRoutine());
        }
    }

    public IEnumerator SphereCastingRoutine()
    {
        while (this.enabled)
        {
            unitList = GetNearbyEnemyUnits(radius);

            if (unitList.Count == 0)
            {
                currentUnit = null;
                OnNoEnemyNearby.Invoke(this);
            }
            else if (!unitList.Contains(currentUnit))
            {
                currentUnit = unitList[0];
                OnNewTarget.Invoke(currentUnit);
            }

            if (includeTowers)
            {
                towerList = GetNearbyEnemyTowers(radius);
                
                if (towerList.Count == 0)
                {
                    currentTower = null;
                }
                else if (!towerList.Contains(currentTower))
                {
                    currentTower = towerList[0];
                }
            }
            yield return new WaitForSeconds(tick);
        }
    }
    

    public List<Unit> GetNearbyEnemyUnits(float mRadius)
    {
        List<Unit> nearbyEnemies = new List<Unit>();

        Collider[] colliders = Physics.OverlapSphere(owner.position, mRadius, layer);

        foreach (Collider col in colliders)
        {
            
            if (col.GetComponent<Unit>())
            {
                Unit unit = col.GetComponent<Unit>();
                if (unit.faction != faction)
                {
                    if (unit.state != UnitState.Die)
                    {
                        nearbyEnemies.Add(unit);
                    }
                }
            }
        }

        return nearbyEnemies;
    }
    
    public List<Unit> GetNearbyEnemyUnits(float mRadius, Faction mFaction)
    {
        List<Unit> nearbyEnemies = new List<Unit>();

        Collider[] colliders = Physics.OverlapSphere(owner.position, mRadius, layer);

        foreach (Collider col in colliders)
        {
            if (col.GetComponent<Unit>())
            {
                Unit unit = col.GetComponent<Unit>();
                if (unit.faction != mFaction)
                {
                    if (unit.state != UnitState.Die)
                    {
                        nearbyEnemies.Add(unit);
                    }
                }
            }
        }

        return nearbyEnemies;
    }

    public List<Unit> GetNearbyFriendlyUnits(float mRadius)
    {
        List<Unit> nearbyFriendlyUnits = new List<Unit>();

        Collider[] colliders = Physics.OverlapSphere(owner.position, mRadius, layer);

        foreach (Collider col in colliders)
        {
            if (col.GetComponent<Unit>())
            {
                Unit unit = col.GetComponent<Unit>();
                
                if (unit.faction == faction)
                {
                    if (unit.state != UnitState.Die)
                    {
                        nearbyFriendlyUnits.Add(unit);
                    }
                }
            }
        }

        return nearbyFriendlyUnits;
    }
    
    public List<Unit> GetNearbyFriendlyUnits(float mRadius, Faction mFaction)
    {
        List<Unit> nearbyFriendlyUnits = new List<Unit>();

        Collider[] colliders = Physics.OverlapSphere(owner.position, mRadius, layer);

        foreach (Collider col in colliders)
        {
            if (col.GetComponent<Unit>())
            {
                Unit unit = col.GetComponent<Unit>();
                
                if (unit.faction == mFaction)
                {
                    if (unit.state != UnitState.Die)
                    {
                        nearbyFriendlyUnits.Add(unit);
                    }
                }
            }
        }

        return nearbyFriendlyUnits;
    }
    
    public List<Tower> GetNearbyEnemyTowers(float mRadius)
    {
        List<Tower> nearbyTowers = new List<Tower>();

        Collider[] colliders = Physics.OverlapSphere(owner.position, mRadius, layer);

        foreach (Collider col in colliders)
        {
            if (col.GetComponent<Tower>())
            {
                Tower tower = col.GetComponent<Tower>();
                if (tower.faction != faction)
                {
                    nearbyTowers.Add(tower);
                }
            }
        }

        return nearbyTowers;
    }

    public bool IsNearbyTower() => currentTower != null;
}
