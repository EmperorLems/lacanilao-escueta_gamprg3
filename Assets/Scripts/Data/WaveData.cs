using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Wave Data", menuName = " WaveData ")]

public class WaveData : ScriptableObject
{
    public List<Creeps> creep;
    public Creeps siegeCreep;
}
