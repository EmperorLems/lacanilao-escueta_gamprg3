using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Stats
{
    public float str;
    public float agi;
    public float _int;
    public int primary;
}

[CreateAssetMenu(fileName = "New Unit", menuName = " UnitData ")]

public class UnitData : ScriptableObject
{
    public new string name;
    public float maxHealth;
    public float maxMana;
    public float healthRegen;
    public float manaRegen;
    public float baseArmor;
    public float magicResist;
    public float moveSpeed;
    public float baseDamage;
    public float attackSpeed;
    public float attackTime;
    public float attackRange;
    public Stats stats = new Stats();
    public Stats growth = new Stats();
    public ArmorData armorData;
    public AttackType attackType;
    public Sprite icon;

}
