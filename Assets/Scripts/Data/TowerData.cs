using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

[CreateAssetMenu(fileName = "New Tower", menuName = " TowerData ")]

public class TowerData : ScriptableObject
{
    public float health;
    public float armor;
    public float damage;
    public float attackSpeed;
    public float armorProtectionBonus;
    public int GoldBounty;

    public ArmorData armorData;
    public AttackType attackType;
}
