using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

[CreateAssetMenu(fileName = "New ArmorData", menuName = " ArmorData ")]

public class ArmorData : ScriptableObject
{
    public ArmorType mArmorType;
    public float basicDmgReceiveMult;
    public float pierceDmgReceiveMult;
    public float siegeDmgReceiveMult;
    public float heroDmgReceiveMult;
}
