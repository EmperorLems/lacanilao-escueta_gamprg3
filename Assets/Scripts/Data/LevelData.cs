using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class LevelProperties
{
    public int requiredXP;
    public int killXP;
    public int goldLastHitBounty;
    public int goldAssistBounty;
    public int respawnTime;
    public bool isBasicAbilityUpgradable;
    public bool isUltimateAbilityUpgradable;
}

[CreateAssetMenu(fileName = "New LevelData", menuName = " LevelData ")]
public class LevelData : ScriptableObject
{
    public LevelProperties[] PropertiesInLevel = new LevelProperties[26];
}
