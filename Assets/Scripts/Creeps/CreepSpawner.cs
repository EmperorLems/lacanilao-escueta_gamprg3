using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepSpawner : MonoBehaviour
{
    [SerializeField] private List<Transform> waypoints;
    [SerializeField] private WaveData waveData;
    [SerializeField] private WaveData megaCreepWaveData;
    [SerializeField] private List<Hero> Heroes_Prefab;
    public List<Barracks> barracks;

    private int currentWaveIndex = 0;
    private bool shouldSpawnSiegeCreep = false;
    private bool hasSpawnedWave = false;
    private bool isBarracksDestroyed = false;

    private void Start()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.OnEnteredFiveMinutes += SpawnSiegeCreep;
        gameManager.OnEntered30Seconds += SpawnWave;
        
        SpawnHero();
    }
    private void Update()
    {
        if (isBarracksDestroyed == false)
        {
            if (barracks.Count <= 0)
            {
                isBarracksDestroyed = true;
                waveData = megaCreepWaveData;
            }
        }
    }

    private void SpawnWave(bool isSiege)
    {
        hasSpawnedWave = false;
        SpawnCreep();
    }

    private void SpawnCreep()
    {
        if (hasSpawnedWave == false)
        {
            foreach (Creeps creep in waveData.creep)
            {
                //CreepMovement spawnedCreep = Instantiate(creep, this.transform).GetComponent<CreepMovement>();
                //spawnedCreep.SetWayPoints(waypoints);
                //spawnedCreep.MoveToWaypoint();
            }
            Creeps spawnedCreep = Instantiate(waveData.creep[currentWaveIndex], transform);
            //spawnedCreep.getController().waypoints = waypoints;
            spawnedCreep.GetComponent<CreepController>().waypoints = waypoints;

            GameManager.instance.getUnitManager().AddCreepToList(spawnedCreep);
            
            StartCoroutine(SpawnCreepDelay());
        }
    }

    private void SpawnSiegeCreep(bool shouldSpawnSiege)
    {
        shouldSpawnSiegeCreep = shouldSpawnSiege;
        SpawnWave(shouldSpawnSiege);
    }

    private IEnumerator SpawnCreepDelay()
    {
        yield return new WaitForSeconds(0.5f);
        if (currentWaveIndex < waveData.creep.Count-1)
        {
            currentWaveIndex++;
            SpawnCreep();
        }
        else
        {
            if (shouldSpawnSiegeCreep)
            {
                Creeps spawnedCreep = Instantiate(waveData.siegeCreep, transform);
                spawnedCreep.GetComponent<CreepController>().waypoints = waypoints;
            }
            currentWaveIndex = 0;
            hasSpawnedWave = true;
            shouldSpawnSiegeCreep = false;
        }
    }

    public void NextWaypoint()
    {

    }

    private void SpawnHero()
    {
        foreach (Hero h in Heroes_Prefab)
        {
            Hero spawned = Instantiate(h, transform.position, transform.rotation);
            spawned.getController().waypoints = waypoints;
            GameManager.instance.getUnitManager().AddHeroToList(spawned.GetComponent<Hero>());
        }
    }

    public List<Transform> Waypoints => waypoints;
}
