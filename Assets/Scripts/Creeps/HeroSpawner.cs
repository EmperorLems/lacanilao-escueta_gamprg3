using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class HeroSpawner : MonoBehaviour
{
    [SerializeField] private int maxHero;
    [SerializeField] private CreepSpawner _creepSpawner;
    private int heroCounter;
    public UnitManager UnitManager;
    [SerializeField] private Faction faction;

    public Hero SpawnHero(Hero prefab, bool isPlayer)
    {
        _creepSpawner.GetComponent<CreepSpawner>();
        prefab.faction = faction;
        Hero hero = Instantiate(prefab, transform);
        if (!isPlayer) hero.GetComponent<HeroController>().waypoints = _creepSpawner.Waypoints;
        UnitManager.AddHeroToList(hero);
        heroCounter++;
        return hero;
    }

    public bool isMaxHero => heroCounter >= maxHero;
}
