using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] private Image healthBar;

    private float currentHealth;
    private float maxHealth;
    private Transform cam;
    public UnityEvent<GameObject> OnDeath;
    
    private void Start()
    {
        healthBar.fillOrigin = 0;
        cam = Camera.main.transform;
    }

    private void LateUpdate()
    {
        healthBar.fillAmount = GetHealthPercentage();
        transform.LookAt(transform.position + cam.forward);
    }

    public float GetCurrentHealth() => currentHealth;
    
    public float GetMaxHealth() => maxHealth;
    public float GetHealthPercentage() => currentHealth / maxHealth;
    
    public void SetMaxHealthWithHeal(float healthVal)
    {
        maxHealth = healthVal;
        currentHealth = maxHealth;
    }

    public void SetMaxHealth(float maxVal)
    {
        maxHealth = maxVal;
        if (maxHealth < currentHealth) currentHealth = maxHealth;
    }
    
    
    public bool IsMaxHealth() => maxHealth <= currentHealth;

    public void TakeDamage(float damage, GameObject attacker)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            OnDeath.Invoke(attacker);
        }
    }

    public void HealHP(float HealVal)
    {
        currentHealth += HealVal;
        if (currentHealth >= maxHealth) currentHealth = maxHealth;
    }
    
    public bool IsHealthDepleted()
    {
        if (currentHealth <= 0)
        {
            return true;
        }
        else return false;
    }
}
