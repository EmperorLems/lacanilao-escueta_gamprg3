using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingManager : MonoBehaviour
{
    [SerializeField] private Light dirLight;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.isDay)
        {
            dirLight.color = Color.white;
        }
        else
        {
            dirLight.color = Color.blue;
        }
    }

    public void EnteredDay()
    {
        
    }
}
