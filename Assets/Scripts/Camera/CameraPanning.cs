﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanning : MonoBehaviour
{
    public float panSpeed;
    public float boarderPixCount;
    public Vector2 panLimit;
    Vector3 pos;

    private Transform player;

    private void Start()
    {
        player = GameManager.instance.Player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        pos = transform.position;
        panning(pos);
    }

    public void panning(Vector3 pos)
    {
        if (Input.mousePosition.y >= Screen.height - boarderPixCount)
        {
            pos.z += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.y <= boarderPixCount)
        {
            pos.z -= panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x >= Screen.width - boarderPixCount)
        {
            pos.x += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x <= boarderPixCount)
        {
            pos.x -= panSpeed * Time.deltaTime;
        }
        pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x);
        pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y);

        transform.position = pos;

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKey(KeyCode.Space))
        {
            transform.position = new Vector3(player.position.x, 0, player.position.z);
        }
    }
}
