using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaComponent : MonoBehaviour
{
    [SerializeField] private Image manaBar;

    private float currentMana;
    private float maxMana;

    private Transform mainCam;
    private void Start()
    {
        manaBar.fillOrigin = 0;
        mainCam = Camera.main.transform;
    }

    private void FixedUpdate()
    {
        manaBar.fillAmount = GetManaPercentage();
        transform.LookAt(transform.position + mainCam.forward);
    }

    public float GetCurrentMana() => currentMana;
    
    public float GetMaxMana() => maxMana;
    
    public float GetManaPercentage() => currentMana / maxMana;
    
    public void SetMaxMana(float manaVal)
    {
        maxMana = manaVal;
        currentMana = maxMana;
    }

    public bool IsMaxMana() => maxMana <= currentMana;
    
    public bool IsManaDepleted()
    {
        if (currentMana <= 0)
        {
            return true;
        }
        else return false;
    }

    public void AddMana(float manaVal)
    {
        currentMana += manaVal;
        if (currentMana > maxMana) currentMana = maxMana;
    }

    public void SubtractMana(float ManaVal)
    {
        currentMana -= ManaVal;
        if (currentMana <= 0) currentMana = 0;
    }
}
